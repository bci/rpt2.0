package com.bcb.rpt.common.vo;

import lombok.Data;
import org.springframework.jmx.export.naming.IdentityNamingStrategy;

@Data
public class StaffWeekSaleVO {
    private String name;
    private Integer  saleNum;
    private Long saleMoney;
}
