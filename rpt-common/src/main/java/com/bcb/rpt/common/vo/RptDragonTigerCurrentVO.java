package com.bcb.rpt.common.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RptDragonTigerCurrentVO {
    private String id;//榜单id
    private Integer type;// 类型 1.协作 2.销售 3.利润 4.跨部门销售 5.满意度（一期没有）
    private String topName;// 榜首姓名
    private Long topCount;//榜首统计
    private String yesterdayName;//昨日最佳姓名
    private Long yesterdayCount;//昨日最佳统计
    private String myName;//我的姓名
    private Long myCount;//我的统计
    private String recollections;//获奖感言
    private Integer editType;//1、可编辑获奖感言 0、不可编辑
    private String month;//上期月份
}
