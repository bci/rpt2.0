package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class WeekShopSaleCurveVO {
    private Integer saleNum;
    private Long saleMoney;
    private String day;
}
