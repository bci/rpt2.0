package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class MonthStaffSaleTopVO {
    private String name;
    private Long saleMoney;
}
