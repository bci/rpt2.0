package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class WeekStaffNameNumVO {
    private Long sale_money;
    private String name;
}
