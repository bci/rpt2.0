package com.bcb.rpt.common.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class RptHighStockWarningVO {

    private Integer highStockNum;
    private BigDecimal percent;
    private List<Goods> goods;

    @Data
    public static class Goods {
        private String goodsName;
        private Integer onStoreNum;
    }
}
