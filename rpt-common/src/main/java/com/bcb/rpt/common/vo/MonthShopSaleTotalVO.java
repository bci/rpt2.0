package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class MonthShopSaleTotalVO {
    private Integer saleNum;
    private Long saleMoney;
    private Long addMoney;
    private Long purchaseExpenses;
    private Long billMoney;
    private String month;
}
