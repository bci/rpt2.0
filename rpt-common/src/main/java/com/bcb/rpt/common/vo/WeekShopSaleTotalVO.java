package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class WeekShopSaleTotalVO {
    private Integer sale_num;
    private Long sale_money;
    private Long add_money;
    private Long purchase_expenses;
    private String name;

}
