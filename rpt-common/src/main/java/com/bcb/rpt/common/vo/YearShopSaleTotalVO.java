package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class YearShopSaleTotalVO {
    private Integer  saleNum;
    private Long saleMoney;
    private Long billMoney;
    private Long purchaseExpenses;
}
