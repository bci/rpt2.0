package com.bcb.rpt.common.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum  IsOrNotEnums {

    IS(1,"是"),
    NOT(0,"否");


    /** 数值*/
    private final Integer code;
    /** 含义*/
    private final String desc;

}
