package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class DayStaffSaleVO {
    private String name;
    private String staff_id;
    private String shop_id;
    private Integer sale_num;
    private Long sale_money;
}
