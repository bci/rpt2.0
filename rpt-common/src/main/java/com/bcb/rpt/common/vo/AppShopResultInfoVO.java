package com.bcb.rpt.common.vo;

import lombok.Data;

import java.util.List;

@Data
public class AppShopResultInfoVO {

    private Integer orderNum;
    private Long saleNumLong;
    private String saleNum;
    private Integer OrderValue;
    private Integer SaleValue;
    private List<Shop> shops;
    @Data
    public static class Shop {
        private String shopId;
        private String shopName;
        private Integer staffNum;
        private Integer orderNum;
        private Long saleNumLong;
        private String saleNum;
        private String shopAddress;
        private Integer shopRank;
        private Integer rankValue;
    }


}
