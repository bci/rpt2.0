package com.bcb.rpt.common.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 统计报表表单
 */
@Data
public class AppStaffResultInfoForm {
//    @NotBlank(message = "token不可为空")
//    private String token;
    @NotBlank(message = "当前登录人所属商户不能为空")
    private String gwMerchantId;
    @NotBlank(message = "当前登录人不是商户员工")
    private String gwMerchantStaffId;
    @NotNull(message = "statisticsType不可为空")
    private Integer statisticsType;//统计类别，0-人员,1-店铺
    private String gwShopId;
    private String shopId;
    @NotNull(message = "rankType不可为空")
    private Integer rankType;//排名类别，0-客单量，1-销售额
    @NotNull(message = "period不可为空")
    private Integer period;// 统计周期，0-日，1-月，2-年
    private Integer orgLvl;// 登陆人数据权限级别，0员工，1高管
}
