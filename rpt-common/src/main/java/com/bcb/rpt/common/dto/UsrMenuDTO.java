package com.bcb.rpt.common.dto;

import lombok.Data;

/**
 * @author: Mr.Yuan
 * @date: 2020-05-08
 * @description: 菜单DTO类
 */
@Data
public class UsrMenuDTO {

    /**
     * id
     */
    private Long id;

    /**
     * 父级ID（对于顶层菜单该字段填0）
     */
    private Long parentId;

    /**
     * 菜单名称
     */
    private String title;

    /**
     * 菜单级数（顶层菜单为0，二级菜单为1，依次类推）
     */
    private Integer level;

    /**
     * 菜单排序（1最高，展现排序最靠前，依次类推）
     */
    private Integer sort;

    /**
     * 前端名称（用于组合菜单对应的URI）
     */
    private String name;

    /**
     * 前端图标
     */
    private String icon;
}
