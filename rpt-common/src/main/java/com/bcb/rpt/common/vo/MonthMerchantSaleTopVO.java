package com.bcb.rpt.common.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@Data
public class MonthMerchantSaleTopVO {
    private Long todayAmountLong;
    private String todayAmount;
    private Integer odrNum;
    private String merchant_id;
    private String merchant_name;
    private BigDecimal vitality;

}
