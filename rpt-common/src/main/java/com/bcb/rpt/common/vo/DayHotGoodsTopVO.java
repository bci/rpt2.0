package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class DayHotGoodsTopVO {
    private String goods_id;
    private String name;
    private Integer sale_num;
    private Long sale_money;
}
