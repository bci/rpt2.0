package com.bcb.rpt.common.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Data
public class MonthPlatTrendVO {
    private String total_money;
    private Integer total_merchant;
    private String day;
}
