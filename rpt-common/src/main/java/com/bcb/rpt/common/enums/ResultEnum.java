package com.bcb.rpt.common.enums;

import lombok.Getter;

/**
 * @author Mr.Yuan
 * @date 2020-04-24
 * @describe 统一返回信息
 */
@Getter
public enum ResultEnum {

    /** 请求成功返回*/
    SUCCESS("000000","请求成功"),
    ERROR("999999","未知错误"),

    /** 参数格式错误,根据规范可再详细划分*/
    PARAM_ERROR_FORMAT("020100","参数格式错误"),

    /** 参数逻辑错误,根据规范可再详细划分*/
    PARAM_ERROR_LOGIC("020200","参数逻辑错误"),
    PARAM_ERROR_LOGIC_SAME_MOBILE("020201","该手机号码已存在"),
    PARAM_ERROR_LOGIC_SAME_ACCOUNT("020202","该账号已存在"),

    /** 用户登陆类错误 */
    LOGIN_ERROR_ACCOUNT_NOT_EXIST("020301", "该账号不存在"),
    LOGIN_ERROR_PASSWORD_WRONG("020302", "密码错误"),
    LOGIN_ERROR_STATUS_ABNORMAL("020303", "该账号状态异常"),

    /** 用户修改密码类错误 */
    PASSWORD_ERROR_MOBILE_PHONE_NOT_EXIST("020401","该手机号码不存在"),
    PASSWORD_ERROR_OLD_PASSWORD_WRONG("020402","老密码错误"),
    PASSWORD_ERROR_TOKEN_INVALID("020403","token不合法"),

    /** 用户模块DB操作类错误 */
    USR_ERROR_DB_QUERY_RECORD_NOT_EXIST("020801","查询的数据不存在"),
    USR_ERROR_DB_DELETE_RECORD_NOT_EXIST("020802","删除的数据不存在"),
    USR_ERROR_DB_DELETE_RECORD_DEPENDED("020803","删除的数据被上层依赖"),
    USR_ERROR_DB_MODIFY_RECORD_NOT_EXIST("020804","修改的数据不存在"),
    USR_ERROR_DB_ADD_RECORD_ALREADY_EXIST("020805","添加的数据已存在"),
    USR_ERROR_DB_ADD_RECORD_DEPENDENCY_LOST("020806","添加的数据所依赖的底层数据不存在"),

    /** 用户模块通用类错误 */
    USR_ERROR_ROLE_NOT_EXIST("020901","目标角色不存在"),
    USR_ERROR_MENU_NOT_EXIST("020902","目标菜单不存在"),
    USR_ERROR_AUTH_NOT_EXIST("020903","目标权限不存在"),


    /** 其他错误*/
    OTHER_ERROR("999999","网络连接错误,请稍后再试"),
    ;


    /** 返回码*/
    private String code;
    /** 提示信息*/
    private String msg;

    ResultEnum(String code, String msg){
        this.code = code;
        this.msg = msg;
    }

}
