package com.bcb.rpt.common.vo;


import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class RptShopSummaryVO {

    /**
     * 关联店铺表id
     */
    private String shopId;

    /**
     * 关联商户表id
     */
    private String merchantId;

    /**
     * 店铺当日总销量
     */
    private Integer saleNum;

    /**
     * 店铺当天总销售额
     */
    private Long saleMoney;

    /**
     * 当日开具发票总额
     */
    private Long billMoney;

    /**
     * 当日开具发票量
     */
    private Integer billNum;

    /**
     * 当日进/购货总支出
     */
    private Long purchaseExpenses;

    /**
     * 当天退货单数
     */
    private Integer refundNum;

    /**
     * 当日退货总额
     */
    private Long refundMoney;

    /**
     * 当日换货总单数
     */
    private Integer exchangeNum;

    /**
     * 当日待换货总额
     */
    private Long exchangeStayMoney;

    /**
     * 当日已换货总额
     */
    private Long exchangeEndMoney;

    /**
     * 当天预售数量
     */
    private Integer preNum;

    /**
     * 当日预售商品总额
     */
    private Long preMoney;

    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
