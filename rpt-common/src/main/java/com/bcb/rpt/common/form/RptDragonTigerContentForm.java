package com.bcb.rpt.common.form;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RptDragonTigerContentForm {
    //    @NotBlank(message = "token不可为空")
//    private String token;
    @NotBlank(message = "当前登录人商户员工id不能为空")
    private String gwMerchantStaffId;

    @NotBlank(message = "month不可为空")
    private String month;

    @NotNull(message = "type类型不可为空")
    private Integer type;

    @NotBlank(message = "content获奖感言不可为空")
    private String content;

}
