package com.bcb.rpt.common.vo;


import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RptStaffHotSaleVO {

    /**
     * 关联员工id
     */
    private String staffId;

    /**
     * 关联商品id
     */
    private String goodsId;

    /**
     * 该商品的总销量
     */
    private Integer totalNum;

    /**
     * 该员工该商品的销售额
     */
    private Long totalAmount;

    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
