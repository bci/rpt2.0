package com.bcb.rpt.common.vo;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RptStaffSaleInfoVO {

    /**
     * 关联店铺表id
     */
    private String shopId;

    /**
     * 关联商户表id
     */
    private String merchantId;

    /**
     * 关联员工id
     */
    private String staffId;

    /**
     * 该员工当天的总销量
     */
    private Integer saleNum;

    /**
     * 该员工当天的销售额
     */
    private Long saleMoney;



    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
