package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class MonthShopSaleDetailVO {
    private String day;
    private Long saleMoney;
    private Integer billNum;
    private Long purchaseExpenses;
    private String totalHotName;
}
