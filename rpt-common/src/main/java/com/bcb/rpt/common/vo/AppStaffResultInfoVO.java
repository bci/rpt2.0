package com.bcb.rpt.common.vo;

import lombok.Data;

import java.util.List;

@Data
public class AppStaffResultInfoVO {

    private String shopName;
    private Integer orderNum;
    private Long saleNumLong;
    private String saleNum;
    private Integer shopRank;

    private List<Staff> staff;
    @Data
    public static class Staff {
        private String shopId;
        private String staffId;
        private String userName;
        private String headImg;
        private Integer orderNum;
        private Long saleNumLong;
        private String saleNum;
        private Integer merchantRank;
        private Integer shopRank;
    }


}
