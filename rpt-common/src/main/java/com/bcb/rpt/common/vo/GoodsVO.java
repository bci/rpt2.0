package com.bcb.rpt.common.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class GoodsVO {

    private String id;

    private String goodsName;

    private String goodsUrl;

    private String price;

    private LocalDateTime createTime;
}
