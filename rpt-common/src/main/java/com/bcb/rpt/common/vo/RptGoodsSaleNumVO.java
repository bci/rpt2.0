package com.bcb.rpt.common.vo;


import lombok.Data;

import java.time.LocalDate;

@Data
public class RptGoodsSaleNumVO {

    /**
     * 关联店铺表id
     */
    private String shopId;

    /**
     * 关联商户表id
     */
    private String merchantId;

    /**
     * 关联品类id
     */
    private String categoryId;

    /**
     * 关联商品id
     */
    private String goodsId;

    /**
     * 该店铺下某商品当天的销量
     */
    private Integer saleNum;

    /**
     * 该店铺下某商品当天的销售额
     */
    private Long saleMoney;

    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
