package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class DayShopSaleProVO {
    private String category_name;
    private Long sale_money;
    private String category_id;
    private String shop_id;
}
