package com.bcb.rpt.common.vo;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

/**
 * @author: Mr.Ma
 * @date: 2020-05-11
 * @description: 用户角色返回参数类
 */
@Data
public class RptPlatformDailySummaryVO {

    /**
     * 平台每日总成交量
     */
    private Integer totalNum;

    /**
     * 平台每日总成交额
     */
    private Long totalMoney;

    /**
     * 平台截至今日总入驻商户数
     */
    private Integer totalMerchant;

    /**
     * 平台截至今日总店铺数
     */
    private Integer totalShop;

    /**
     * 每日微信服务商返利
     */
    private Long weixinRebate;

    /**
     * 每日阿里服务商返利
     */
    private Long aliPayRebate;

    /**
     * 平台每日增值服务收入汇总
     */
    private Long serviceIncome;

    /**
     * 平台每日支付汇总
     */
    private Long platfromPay;

    /**
     * 具体统计数据的归属时间
     */
    private LocalDate date;
}
