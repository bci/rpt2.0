package com.bcb.rpt.common.vo;
import lombok.Data;

@Data
public class DayStockGoodsHighVO {
    private String goods_id;
    private String goods_name;
    private Integer on_store_num;
    private Integer lately_sale_num;
}
