package com.bcb.rpt.common.vo;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class RptPlatformCategoryInfoVO {


    private String name;

    private String value;



    /**
     * 该品类商品的总销量
     */
//    private Integer totalNum;

//    private LocalDate date;
}
