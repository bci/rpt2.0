package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class WeekStockGoodsHighVO {
    private String goods_id;
    private String goods_name;
    private Integer on_store_num;
}
