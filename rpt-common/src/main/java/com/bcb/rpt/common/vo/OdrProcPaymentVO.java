package com.bcb.rpt.common.vo;

import lombok.Data;
import java.time.LocalDateTime;

@Data
public class OdrProcPaymentVO {

    /**
     * 订单总价款
     */
    private Long totalAmount;

    /**
     * 关联订单id
     */
    private String orderId;

    /**
     * 订单支付类别，1在线支付，2货到付款，3现场支付
     */
    private Integer payType;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;
}
