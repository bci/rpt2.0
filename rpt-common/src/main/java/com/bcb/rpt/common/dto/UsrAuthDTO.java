package com.bcb.rpt.common.dto;

import lombok.Data;

/**
 * @author: Mr.Yuan
 * @date: 2020-05-08
 * @description: 权限DTO类
 */
@Data
public class UsrAuthDTO {

    /**
     * id
     */
    private Long id;

    /**
     * 归属系统类型（1：终端app；2：商户ERP；3：平台ERP）
     */
    private Integer systemType;

    /**
     * 权限名称，最大长度100
     */
    private String name;

    /**
     * 权限描述，最大长度255
     */
    private String description;

    /**
     * 权限url路径
     */
    private String url;

    /**
     * 权限分类ID
     */
    private Long categoryId;
}
