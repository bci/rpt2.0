package com.bcb.rpt.common.vo;


import lombok.Data;

@Data
public class MerchantSaleTopVO {
    private String todayAmount;
    private String merchant_id;
    private String merchant_name;
}
