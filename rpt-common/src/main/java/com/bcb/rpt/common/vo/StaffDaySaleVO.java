package com.bcb.rpt.common.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StaffDaySaleVO {
    private String day;
    private Integer saleNum;
    private Long saleMoney;
    private BigDecimal vitality;
}
