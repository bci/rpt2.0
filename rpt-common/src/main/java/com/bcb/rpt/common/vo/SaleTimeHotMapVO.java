package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class SaleTimeHotMapVO {
    private String shop_id;
    private Integer total_goods_quantity;
    private Long total_amount;
    private String hour;
}
