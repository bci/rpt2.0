package com.bcb.rpt.common.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PlatIncomePayVO {
//    private Long wei_xin;
//    private Long ali;
//    private Long service_income;
    private String month_income;
    private String month_pay;
    private String month;
}
