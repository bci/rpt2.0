package com.bcb.rpt.common.vo;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class RptPlatformCategoryInfoResultVO {
    private List<String> data;
    private List<RptPlatformCategoryInfoVO> content;
}
