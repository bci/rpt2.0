package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class StaffHotGoodsMoneyTopVO {
    private String goodsName;
    private Long totalAmount;
}
