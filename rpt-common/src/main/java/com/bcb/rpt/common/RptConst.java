package com.bcb.rpt.common;

/**
 * @author: Mr.Yuan
 * @date: 2020-04-29
 * @description: 用户模块常量定义类
 */
public class RptConst {

    /** 用户状态（-1:删除,0:禁用,1:可用） */
    public static final int USER_STATUS_DELETED = -1;
    public static final int USER_STATUS_DISABLE = 0;
    public static final int USER_STATUS_NORMAL  = 1;

    /** 短信验证码长度范围 */
    public static final int USER_SMS_CODE_LEN_MIN = 4;
    public static final int USER_SMS_CODE_LEN_MAX = 6;

    /** 其他字段长度范围 */
    public static final int USER_HEAD_IMG_LEN_MAX = 255;
    public static final int USER_NAME_LEN_MAX = 128;
    public static final int USER_NICK_NAME_LEN_MAX = 128;
    public static final int USER_ACCOUNT_LEN_MAX = 128;

    /** 表ID前缀 */
    public static final String USER_TABLE_ID_PRE_USER = "usr";
    public static final String USER_TABLE_ID_PRE_ACCOUNT = "act";
    public static final String USER_TABLE_ID_PRE_LOGIN_LOG = "llg";


    /** 常用正则表达式*/
    public static final String USER_REGEXP_MOBILE_PHONE = "^[1]([3-9])[0-9]{9}$";

}
