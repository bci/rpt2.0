package com.bcb.rpt.common.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RptDragonTigerHistoryVO {
    private Integer type;// 类型 1.协作 2.销售 3.利润 4.跨部门销售 5.满意度（一期没有）
    private String topName;// 历史榜首姓名
    private Long topCount;//历史榜首统计
    private String myName;//历史我的姓名
    private Long myCount;//历史我的统计
    private String recollections;//获奖感言


}
