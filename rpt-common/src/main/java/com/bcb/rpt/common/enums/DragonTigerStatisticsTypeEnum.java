package com.bcb.rpt.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.YearMonth;

@Getter
@AllArgsConstructor
public enum DragonTigerStatisticsTypeEnum {
    //类型 1.协作 2.销售 3.利润 4.跨部门销售 5.满意度（一期没有）
    COLLABORATE(1, "协作", 9,"collaboration"),
    SALEROOM(2, "销售", 19,"saleMoney"),
    PROFIT(3, "利润", -1,"profitMoney"),
    CROSS(4, "跨部门销售", -1,"crossDepartmentSales");
    /**
     * 数值
     */
    private final Integer code;
    /**
     * 含义
     */

    String desc;
    // 统计日截值
    private final Integer boundary;
    // 统计值实体属性名称
    private final String entityAttributeName;
}
