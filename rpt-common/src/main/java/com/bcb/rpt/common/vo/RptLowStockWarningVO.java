package com.bcb.rpt.common.vo;


import lombok.Data;

import java.time.LocalDate;

@Data
public class  RptLowStockWarningVO {

    /**
     * 关联商品id
     */
    private String goodsId;

    private String goodsName;

    /**
     * 关联商户表id
     */
    private String merchantId;

    /**
     * 关联店铺表id
     */
    private String shopId;

    /**
     * 该商品目前在库数量
     */
    private Integer onStoreNum;

    /**
     * 该商品最近时间的的销量
     */
    private Integer latelySaleNum;

    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
