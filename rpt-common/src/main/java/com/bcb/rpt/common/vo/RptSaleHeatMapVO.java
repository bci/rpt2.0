package com.bcb.rpt.common.vo;


import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RptSaleHeatMapVO {
    /**
     * 关联店铺表id
     */
    private String shopId;

    /**
     * 该店铺每小时的销量
     */
    private Integer totalGoodsQuantity;

    /**
     * 该店铺每小时的销售额
     */
    private Long totalAmount;

    /**
     * 该条数据的归属时间
     */
    private LocalDateTime date;
}
