package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class StaffHotGoodsTopNumVO {
    private String goodsName;
    private Integer totalNum;
}
