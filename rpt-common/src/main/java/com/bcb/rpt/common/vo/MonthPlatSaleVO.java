package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class MonthPlatSaleVO {
    private String month_total_money;
    private String month;
}
