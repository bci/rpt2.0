package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class YearShopSaleCurveVO {
    private Long saleMoney;
    private Integer saleNum;
    private String month;
}
