package com.bcb.rpt.common.vo;

import lombok.Data;

@Data
public class YearShopSaleDetailVO {
    private String mon;
    private Long saleMoney;
    private Integer billNum;
    private Long purchaseExpenses;
    private String totalHotName;
}
