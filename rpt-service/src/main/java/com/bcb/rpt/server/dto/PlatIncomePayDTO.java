package com.bcb.rpt.server.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author Mr.Li
 * @description: 平台收入支出情况统计
 * @date 2020-07-07 16:09
 */


@Getter
@Setter
@ToString
public class PlatIncomePayDTO {
    private Long wei_xin;
    private Long ali;
    private Long service_income;
    private Long month_incomeLong;
    private String month_income;
    private Long month_payLong;
    private String month_pay;
    private String month;
}
