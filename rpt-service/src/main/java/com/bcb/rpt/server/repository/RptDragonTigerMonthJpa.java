package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptDragonTigerMonth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.YearMonth;

public interface
RptDragonTigerMonthJpa extends JpaRepository<RptDragonTigerMonth, String>,
        JpaSpecificationExecutor<RptDragonTigerMonth> {
    RptDragonTigerMonth findAllByStaffIdAndMonth(String staffId, String his_Month);
}
