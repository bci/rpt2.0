package com.bcb.rpt.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Mr.Li
 * @description: 平台商户当天成交额排行
 * @date 2020-07-07 16:09
 */

@Getter
@Setter
@ToString
public class MerchantSaleTopDTO {
    private Long todayAmountLong;
    private String todayAmount;
    private String merchant_id;
    private String merchant_name;
}
