package com.bcb.rpt.server.aop;

import com.bcb.commom.log.ControllerLog;
import com.bcb.commom.log.ControllerLogHandler;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: Mr.Yuan
 * @date: 2020-07-21
 * @description: Controller层接口访问日志记录切面
 */
@Aspect
@Order(1)
@Component
@Slf4j
public class ControllerLogAspect {
    // 修改成你自己的值
    private final static String point = "execution(public * com.bcb.rpt.server.controller.*.*(..))";
    private final long dataCenterId = 10L;
    private final long machineId = 3;

    private ControllerLogHandler controllerLogHandler;

    private static String serviceName;

    /**
     * 如此写，是为了保证ControllerLogHandler构造之前，serviceName读到了配置项。
     */
    @Value("${spring.application.name}")
    private void setServiceName(String serviceName) {
        ControllerLogAspect.serviceName = serviceName;
        this.controllerLogHandler = new ControllerLogHandler(serviceName, dataCenterId, machineId);

        // 修改成你自己想要过滤的输入参数
        List<Pair<String, String>> l = new ArrayList<>();
        l.add(new Pair<>("setPassword", "java.lang.String"));
        this.controllerLogHandler.addFilterForm("com.bcb.user.common.form.UsrLoginByPasswordForm", l);

        this.controllerLogHandler.addFilterSimpleParameter("gwSessionId", "java.lang.String");

        // 修改成你自己想要过滤的输出结果
        List<String> l2 = new ArrayList<>();
        l2.add("token");
        this.controllerLogHandler.addFilterResult("UsrLoginVO", l2);
    }

    @Around(point)
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        // Before
        long startTime = System.currentTimeMillis();

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        // ing & After
        Object result = null;
        try {
            result = joinPoint.proceed();
        } catch (Exception e) {
            result = e.toString();
            throw e;
        } catch (Throwable throwable) {
            result = throwable.toString();
            throw throwable;
        } finally {
            ControllerLog controllerLog = controllerLogHandler.createLog(joinPoint, startTime, request, result);
            if (null != controllerLog) {
                /** 修改成你自己的实现 **/
                log.info("ControllerLog : {}", controllerLog.toString());
            }
        }

        return result;
    }
}
