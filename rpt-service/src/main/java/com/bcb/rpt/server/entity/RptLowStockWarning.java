package com.bcb.rpt.server.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: 低库存商品预警表 ( rpt_low_stock_warning )
 * @date 2020-07-07 16:21
 */

@Data // lombok注解 生成实体类方法
@Entity // jpa注解 表明是一个实体类 即一张表
@DynamicInsert // jpa注解 null值不插入
@EntityListeners(AuditingEntityListener.class) // 审计功能注解 自动填充时间等信息
public class RptLowStockWarning {
    /**
     * id
     */
    @Id
    private String id;

    /**
     * 关联商品id
     */
    private String goodsId;

    /**
     * 关联商户表id
     */
    private String merchantId;

    /**
     * 关联店铺表id
     */
    private String shopId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 该商品目前在库数量
     */
    private Integer onStoreNum;

    /**
     * 该商品最近时间的的销量
     */
    private Integer latelySaleNum;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 版本控制锁（乐观锁）,默认值0
     */
    private Integer version;

    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
