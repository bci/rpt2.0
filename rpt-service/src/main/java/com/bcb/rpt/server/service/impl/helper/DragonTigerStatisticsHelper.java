package com.bcb.rpt.server.service.impl.helper;

import com.bcb.rpt.common.enums.DragonTigerStatisticsTypeEnum;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

public class DragonTigerStatisticsHelper {

    public static String getPreviousMonthForCollaborate() {
        int day = LocalDate.now().getDayOfMonth();
        if (day > DragonTigerStatisticsTypeEnum.COLLABORATE.getBoundary()) {
            return YearMonth.now().format(DateTimeFormatter.ofPattern("yyyy-MM"));
        } else {
            return YearMonth.now().minusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM"));
        }
    }

    public static String getPreviousMonthForSale() {
        int day = LocalDate.now().getDayOfMonth();
        if (day > DragonTigerStatisticsTypeEnum.SALEROOM.getBoundary()) {
            return YearMonth.now().format(DateTimeFormatter.ofPattern("yyyy-MM"));
        } else {
            return YearMonth.now().minusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM"));
        }
    }

    public static String getPreviousMonthForProfit() {
        return YearMonth.now().minusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM"));
    }

    public static String getPreviousMonthForCross() {
        return YearMonth.now().minusMonths(1).format(DateTimeFormatter.ofPattern("yyyy-MM"));
    }
}
