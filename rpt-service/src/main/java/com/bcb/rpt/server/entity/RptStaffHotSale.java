package com.bcb.rpt.server.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: 员工热卖商品榜统计表 ( rpt_staff_hot_sale )
 * @date 2020-07-07 16:39
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)

public class RptStaffHotSale {

    /**
     * id
     */
    @Id
    private String id;

    /**
     * 关联员工id
     */
    private String staffId;

    /**
     * 关联商品id
     */
    private String goodsId;

    /**
     * 该商品的总销量
     */
    private Integer totalNum;

    /**
     * 该员工该商品的销售额
     */
    private Long totalAmount;

    /**
     * 关联商品名称
     */
    private String goodsName;

    /**
     * 关联员工姓名
     */
    private String staffName;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 版本控制锁（乐观锁）,默认值0
     */
    private Integer version;

    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
