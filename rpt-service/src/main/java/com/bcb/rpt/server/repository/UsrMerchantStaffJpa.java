package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.UsrMerchantStaff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface UsrMerchantStaffJpa extends JpaRepository<UsrMerchantStaff, String> {
    // 人员+店铺全部+订单量
    @Query(nativeQuery = true, value = "SELECT " +
            "t1.shop_id shopId," +
            "t1.id staffId," +
            "t2.name userName," +
            "t2.head_img headImg," +
            "ifnull(t3.orderNum,0) orderNum," +
            "ifnull(t3.saleNum,0) saleNum," +
            "dense_rank ( ) over ( ORDER BY orderNum DESC ) AS merchantRank," +
            "dense_rank ( ) over ( PARTITION BY t1.shop_id ORDER BY orderNum DESC ) AS shopRank " +
            "FROM" +
            " mct_shop t0 " +
            "INNER JOIN usr_merchant_staff t1 ON t1.shop_id = t0.id" +
            " INNER JOIN usr_user t2 ON t1.user_id = t2.id" +
            " LEFT JOIN (" +
            "SELECT " +
            "o1.staff_id," +
            "count( * ) orderNum," +
            "sum( o2.total_price ) saleNum " +
            "FROM " +
            "odr_usr_salesman o1," +
            "odr_proc_payment o2 " +
            "WHERE " +
            "o1.order_id = o2.order_id " +
            "AND o1.create_time BETWEEN :startDate " +
            "AND :endDate " +
            "GROUP BY " +
            "o1.staff_id " +
            ") t3 ON t3.staff_id = t1.id " +
            "WHERE" +
            " t0.merchant_id = :merchantId")
    List<Map<String, Object>> getAppStaffAllOrderTop(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate,
                                                     @Param("merchantId") String merchantId);

    // 人员+店铺全部+销售额
    @Query(nativeQuery = true, value = "SELECT " +
            "t1.shop_id shopId," +
            "t1.id staffId," +
            "t2.name userName," +
            "t2.head_img headImg," +
            "ifnull(t3.orderNum,0) orderNum," +
            "ifnull(t3.saleNum,0) saleNum," +
            "dense_rank ( ) over ( ORDER BY saleNum DESC ) AS merchantRank," +
            "dense_rank ( ) over ( PARTITION BY t1.shop_id ORDER BY saleNum DESC ) AS shopRank " +
            "FROM" +
            " mct_shop t0 " +
            "INNER JOIN usr_merchant_staff t1 ON t1.shop_id = t0.id" +
            " INNER JOIN usr_user t2 ON t1.user_id = t2.id" +
            " LEFT JOIN (" +
            "SELECT " +
            "o1.staff_id," +
            "count( * ) orderNum," +
            "sum( o2.total_price ) saleNum " +
            "FROM " +
            "odr_usr_salesman o1," +
            "odr_proc_payment o2 " +
            "WHERE " +
            "o1.order_id = o2.order_id " +
            "AND o1.create_time BETWEEN :startDate " +
            "AND :endDate " +
            "GROUP BY " +
            "o1.staff_id " +
            ") t3 ON t3.staff_id = t1.id " +
            "WHERE" +
            " t0.merchant_id = :merchantId")
    List<Map<String, Object>> getAppStaffAllSaleTop(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate,
                                                    @Param("merchantId") String merchantId);


    // 店铺+客单量
    @Query(nativeQuery = true, value = "SELECT " +
            "t0.id shopId, " +
            "t0.name shopName, " +
            "t0.address shopAddress, " +
            "ifnull((select count(*) from usr_merchant_staff where shop_id=t0.id), 0) staffNum, " +
            "ifnull(t3.orderNum,0) orderNum, " +
            "ifnull(t3.saleNum,0) saleNum, " +
            "dense_rank ( ) over ( ORDER BY orderNum DESC ) AS shopRank  " +
            "FROM " +
            " mct_shop t0 " +
            "LEFT JOIN(" +
            "SELECT " +
            "o3.shop_id, " +
            "sum( o3.orderNum ) orderNum, " +
            "sum( o3.saleNum ) saleNum  " +
            "FROM " +
            "(" +
            "SELECT " +
            "o0.shop_id, " +
            "o0.id staff_id, " +
            "count( * ) orderNum, " +
            "sum( o2.total_price ) saleNum  " +
            "FROM " +
            "usr_merchant_staff o0, " +
            "odr_usr_salesman o1, " +
            "odr_proc_payment o2  " +
            "WHERE " +
            "o0.id = o1.staff_id  " +
            "AND o1.order_id = o2.order_id  " +
            "AND o1.create_time BETWEEN :startDate " +
            "AND :endDate " +
            "GROUP BY " +
            "o0.shop_id, " +
            "o0.id  " +
            ") o3 " +
            "GROUP BY " +
            "o3.shop_id  " +
            " ) t3 ON t3.shop_id = t0.id  " +
            "WHERE " +
            "t0.merchant_id = :merchantId")
    List<Map<String, Object>>getAppShopAllOrder(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate,
                                                @Param("merchantId") String merchantId);

    // 店铺+销售量
    @Query(nativeQuery = true, value = "SELECT " +
            "t0.id shopId, " +
            "t0.name shopName, " +
            "t0.address shopAddress, " +
            "ifnull((select count(*) from usr_merchant_staff where shop_id=t0.id), 0) staffNum, " +
            "ifnull(t3.orderNum,0) orderNum, " +
            "ifnull(t3.saleNum,0) saleNum, " +
            "dense_rank ( ) over ( ORDER BY saleNum DESC ) AS shopRank  " +
            "FROM " +
            " mct_shop t0 " +
            "LEFT JOIN(" +
            "SELECT " +
            "o3.shop_id, " +
            "sum( o3.orderNum ) orderNum, " +
            "sum( o3.saleNum ) saleNum  " +
            "FROM " +
            "(" +
            "SELECT " +
            "o0.shop_id, " +
            "o0.id staff_id, " +
            "count(*) orderNum, " +
            "sum( o2.total_price ) saleNum  " +
            "FROM " +
            "usr_merchant_staff o0, " +
            "odr_usr_salesman o1, " +
            "odr_proc_payment o2  " +
            "WHERE " +
            "o0.id = o1.staff_id  " +
            "AND o1.order_id = o2.order_id  " +
            "AND o1.create_time BETWEEN :startDate " +
            "AND :endDate " +
            "GROUP BY " +
            "o0.shop_id, " +
            "o0.id  " +
            ") o3 " +
            "GROUP BY " +
            "o3.shop_id  " +
            " ) t3 ON t3.shop_id = t0.id  " +
            "WHERE " +
            "t0.merchant_id = :merchantId")
    List<Map<String, Object>>getAppShopAllSale(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate,
                                                @Param("merchantId") String merchantId);

}
