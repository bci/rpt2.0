package com.bcb.rpt.server.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: 订单销售员表 ( odr_usr_salesman )
 * @date 2020-07-07 16:09
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class OdrOrder {

    /**
     * id
     */
    @Id
    private String id;

    /**
     * 购买人id
     */
    private String buyerId;

    /**
     * 订单来源
     */
    private Integer orderSource;

    /**
     * 订单类型
     */
    private Integer orderType;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 备注
     */
    private String memo;

    /**
     * 商户id
     */
    private String merchantId;

    /**
     * 店铺id
     */
    private String shopId;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;
}
