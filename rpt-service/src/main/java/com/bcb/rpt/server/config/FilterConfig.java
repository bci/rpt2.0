package com.bcb.rpt.server.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author chiangtaol
 * @date 2020-06-30
 * @describe
 */
@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean gatewayGeneratedHeadersFilterRegistrationBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean(new GatewayGeneratedHeadersFilter());
        registration.addUrlPatterns("/*");
        // 值越小越靠前，此处配置有效
        registration.setOrder(1);
        return registration;
    }
}
