package com.bcb.rpt.server.controller;

import com.bcb.commom.util.MoneyUtils;
import com.bcb.commom.vo.ResultVO;
import com.bcb.rpt.common.form.AppStaffResultInfoForm;
import com.bcb.rpt.common.vo.*;
import com.bcb.rpt.server.service.RptAdminService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: Mr.Yuan
 * @date: 2020-05-11
 * @description: 报表管理
 */
@RestController
@Slf4j
@Validated
@RequestMapping("rpt")
public class RptAdminController {
    @Resource
    RptAdminService rptAdminService;

    //  平台每日交易信息统计
    @RequestMapping(value = "/getPlatformDailySummary")
    public ResultVO getPlatformDailySummary(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return ResultVO.success(rptAdminService.getPlatformDailySummary(date));
    }

    //  记录平台内销售商品所属分类，主要是汇总扇形占比图
    @RequestMapping(value = "/getPlatformCategoryInfo")
    public ResultVO getPlatformCategoryInfo(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        List<RptPlatformCategoryInfoVO> info = rptAdminService.getPlatformCategoryInfo(date);
        RptPlatformCategoryInfoResultVO vo = new RptPlatformCategoryInfoResultVO();
        vo.setContent(info);
        vo.setData(info.stream().map(RptPlatformCategoryInfoVO::getName).distinct().sorted().collect(Collectors.toList()));
        return ResultVO.success(vo);
    }

    //  店铺每日汇总
    @RequestMapping(value = "/getShopSummary")
    public ResultVO getShopSummary(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                   @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getShopSummary(date, shopId));
    }

    //  店铺每日各个商品销售量
    @RequestMapping(value = "/getGoodsSaleNum")
    public ResultVO getGoodsSaleNum(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                    @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getGoodsSaleNum(date, shopId));
    }

    ////   周 高库存商品预警
    @RequestMapping(value = "/getWeekHighStockWarning")
    public ResultVO getWeekHighStockWarning(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                            @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getWeekHighStockWarning(date, shopId));
    }

    //    高库存商品预警
    @RequestMapping(value = "/getHighStockWarning")
    public ResultVO getHighStockWarning(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                        @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getHighStockWarning(date, shopId));
    }

    //  低库存商品预警
    @RequestMapping(value = "/getLowStockWarning")
    public ResultVO getLowStockWarning(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                       @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getLowStockWarning(date, shopId));
    }

    //  店铺销售热力图
    @RequestMapping(value = "/getSaleHeatMap")
    public ResultVO getSaleHeatMap(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime date,
                                   @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getSaleHeatMap(date, shopId));
    }

    //  员工热卖商品榜统计
    @RequestMapping(value = "/getStaffHotSale")
    public ResultVO getStaffHotSale(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                    @NotBlank(message = "staffId不能为空") String staffId) {
        return ResultVO.success(rptAdminService.getStaffHotSale(date, staffId));
    }

    //  店铺每日员工销售情况统计
    @RequestMapping(value = "/getStaffSaleInfo")
    public ResultVO getStaffSaleInfo(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                     @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getStaffSaleInfo(date, shopId));
    }

    //  平台每日实时成交额
    @RequestMapping(value = "/getRealTimeAmount")
    public ResultVO getRealTimeAmount(@NotNull(message = "updateTime不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate updateTime) {
        return ResultVO.success(MoneyUtils.handleMoney(rptAdminService.getRealTimeAmount(updateTime)));
    }

    //  平台每日实时入驻商户数量
    @RequestMapping(value = "/getRealTimeMctMerchant")
    public ResultVO getRealTimeMctMerchant(@NotNull(message = "updateTime不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate updateTime) {
        return ResultVO.success(rptAdminService.getRealTimeMctMerchant(updateTime));
    }

    //  平台销售情况汇总
    @RequestMapping(value = "/getPlatFormSaleStat")
    public ResultVO getPlatFormSaleStat(@NotNull(message = "updateTime不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate updateTime) {
        return ResultVO.success(rptAdminService.getPlatFormSaleStat(updateTime));
    }

    //  平台商户情况统计
    @RequestMapping(value = "/getPlatFormMerchantStat")
    public ResultVO getPlatFormMerchantStat(@NotNull(message = "updateTime不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate updateTime) {
        return ResultVO.success(rptAdminService.getPlatFormMerchantStat(updateTime));
    }

    //  平台月度成交额统计
    @RequestMapping(value = "/getMonthPlatSale")
    public ResultVO getMonthPlatSale(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        List<MonthPlatSaleVO> monthPlatSale = rptAdminService.getMonthPlatSale(date);
        Map<String, List<Object>> result = new HashMap<>();
        result.put("month", monthPlatSale.stream().map(MonthPlatSaleVO::getMonth).collect(Collectors.toList()));
        result.put("totalMoney", monthPlatSale.stream().map(MonthPlatSaleVO::getMonth_total_money).collect(Collectors.toList()));
        return ResultVO.success(result);
    }

    //  平台收入支出情况统计
    @RequestMapping(value = "/getPlatIncomePay")
    public ResultVO getPlatIncomePay(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        List<PlatIncomePayVO> platIncomePay = rptAdminService.getPlatIncomePay(date);
        Map<String, List<Object>> result = new HashMap<>();
        result.put("month", platIncomePay.stream().map(PlatIncomePayVO::getMonth).collect(Collectors.toList()));
        result.put("monthIncome", platIncomePay.stream().map(PlatIncomePayVO::getMonth_income).collect(Collectors.toList()));
        result.put("monthPay", platIncomePay.stream().map(PlatIncomePayVO::getMonth_pay).collect(Collectors.toList()));
        return ResultVO.success(result);
    }

    //  平台商户当天成交额排行
    @RequestMapping(value = "/getMerchantSaleTop")
    public ResultVO getMerchantSaleTop(@NotNull(message = "updateTime不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate updateTime) {
        return ResultVO.success(rptAdminService.getMerchantSaleTop(updateTime));
    }

    //  app龙虎榜列表
    @RequestMapping(value = "/dragonTiger/getDragonTigerList")
    public ResultVO getDragonTigerList(@NotBlank(message = "当前登录人不是商户员工") String gwMerchantStaffId,
                                       @NotBlank(message = "当前登录人所属商户不能为空") String gwMerchantId) {
        return ResultVO.success(rptAdminService.getDragonTigerList(gwMerchantStaffId, gwMerchantId));
    }

    //  app历史榜单
    @RequestMapping(value = "/dragonTiger/getHistoryList")
    public ResultVO getHistoryList(@NotBlank(message = "当前登录人不是商户员工") String gwMerchantStaffId,
                                   @NotBlank(message = "当前登录人所属商户不能为空") String gwMerchantId,
                                   @NotNull(message = "hisMonth不能为空") YearMonth hisMonth) {
        return ResultVO.success(rptAdminService.getHistoryList(gwMerchantStaffId, gwMerchantId, hisMonth));
    }

    // app 统计报表 员工
    @RequestMapping(value = "/getAppStaffAndShopResultStaffInfo")
    public ResultVO getAppStaffAndShopResultSeniorInfo(@Validated AppStaffResultInfoForm appStaffResultInfoForm) {
        appStaffResultInfoForm.setOrgLvl(0);
        return ResultVO.success(rptAdminService.getAppStaffAndShopResultInfo(appStaffResultInfoForm));
    }

    // app 统计报表 高管
    @RequestMapping(value = "/getAppStaffAndShopResultSeniorInfo")
    public ResultVO getAppStaffAndShopResultStaffInfo(@Validated AppStaffResultInfoForm appStaffResultInfoForm) {
        appStaffResultInfoForm.setOrgLvl(1);
        return ResultVO.success(rptAdminService.getAppStaffAndShopResultInfo(appStaffResultInfoForm));
    }


    //  平台月度总览
    @RequestMapping(value = "/getMonthPlatList")
    public ResultVO getMonthPlatList(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return ResultVO.success(rptAdminService.getMonthPlatList(date));
    }

    //  平台月度成交额/商户数走势图/每日详情表
    @RequestMapping(value = "/getMonthPlatTrend")
    public ResultVO getMonthPlatTrend(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        List<MonthPlatTrendVO> monthPlatTrend = rptAdminService.getMonthPlatTrend(date);
        Map<String, List<Object>> result = new HashMap<>();
        result.put("day", monthPlatTrend.stream().map(MonthPlatTrendVO::getDay).collect(Collectors.toList()));
        result.put("totalMoney", monthPlatTrend.stream().map(MonthPlatTrendVO::getTotal_money).collect(Collectors.toList()));
        result.put("merchantNum", monthPlatTrend.stream().map(MonthPlatTrendVO::getTotal_merchant).collect(Collectors.toList()));
        return ResultVO.success(result);
    }

    // 每日详情表
    @RequestMapping(value = "/getMonthPlatTrendSale")
    public ResultVO getMonthPlatTrendSale(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return ResultVO.success(rptAdminService.getMonthPlatTrend(date));
    }


    // 平台月度商户成交额排行
    @RequestMapping(value = "getMonthMerchantSaleTop")
    public ResultVO getMonthMerchantSaleTop(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        YearMonth y = YearMonth.of(date.getYear(), date.getMonth().getValue());
        return ResultVO.success(rptAdminService.getMonthMerchantSaleTop(y));
    }

    // 店铺每日销售统计
    @RequestMapping(value = "/getDayShopSaleTotal")
    public ResultVO getDayShopSaleTotal(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                        @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getDayShopSaleTotal(date, shopId));
    }

    // 店铺每日热销商品榜
    @RequestMapping(value = "/getDayHotGoodsTop")
    public ResultVO getDayHotGoodsTop(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                      @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getDayHotGoodsTop(date, shopId));
    }

    // 店铺员工每天销售情况统计
    @RequestMapping(value = "/getDayStaffSale")
    public ResultVO getDayStaffSale(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                    @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getDayStaffSale(date, shopId));
    }

    //店铺销售/时间热力图
    @RequestMapping(value = "/getSaleTimeHotMap")
    public ResultVO getSaleTimeHotMap(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                      @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getSaleTimeHotMap(date, shopId));
    }

    // 店铺每日产品销售额占比
    @RequestMapping(value = "/getDayShopSalePro")
    public ResultVO getDayShopSalePro(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                      @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getDayShopSalePro(date, shopId));
    }

    // 店铺每日商品日销高库存
    @RequestMapping(value = "/getStockGoodsHigh")
    public ResultVO getStockGoodsHigh(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                      @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getStockGoodsHigh(date, shopId));
    }

    // 店铺每周销售统计
    @RequestMapping(value = "/getWeekShopSaleTotal")
    public ResultVO getWeekShopSaleTotal(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                         @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getWeekShopSaleTotal(date, shopId));
    }

    // 店铺每周热销商品榜
    @RequestMapping(value = "/getWeekHotGoodsTop")
    public ResultVO getWeekHotGoodsTop(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                       @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getWeekHotGoodsTop(date, shopId));
    }

    // 每周店铺销售走势图
    @RequestMapping(value = "/getWeekShopSaleCurve")
    public ResultVO getWeekShopSaleCurve(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                         @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getWeekShopSaleCurve(date, shopId));
    }

    // 店铺每周产品销售额占比
    @RequestMapping(value = "/getWeekShopSalePro")
    public ResultVO getWeekShopSalePro(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                       @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getWeekShopSalePro(date, shopId));
    }

    // 店铺每周店员销售统计
    @RequestMapping(value = "/getWeekStaffNameNum")
    public ResultVO getWeekStaffNameNum(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                        @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getWeekStaffNameNum(date, shopId));
    }

    // 店铺每周商品日销高库存
    @RequestMapping(value = "/getWeekStockGoodsHigh")
    public ResultVO getWeekStockGoodsHigh(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                          @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getWeekStockGoodsHigh(date, shopId));
    }

    // 店铺每月销售统计
    @RequestMapping(value = "/getMonthShopSaleTotal")
    public ResultVO getMonthShopSaleTotal(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                          @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getMonthShopSaleTotal(date, shopId));
    }

    // 店铺月度每日营业额走势图
    @RequestMapping(value = "/getMonthShopSaleCurve")
    public ResultVO getMonthShopSaleCurve(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                          @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getMonthShopSaleCurve(date, shopId));
    }

    // 店铺月度每日销售详情
    @RequestMapping(value = "/getMonthShopSaleDetail")
    public ResultVO getMonthShopSaleDetail(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                           @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getMonthShopSaleDetail(date, shopId));
    }

    // 每月员工业绩排行榜
    @RequestMapping(value = "/getMonthStaffSaleTop")
    public ResultVO getMonthStaffSaleTop(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                         @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getMonthStaffSaleTop(date, shopId));
    }

    // 店铺年度销售统计
    @RequestMapping(value = "/getYearShopSaleTotal")
    public ResultVO getYearShopSaleTotal(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                         @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getYearShopSaleTotal(date, shopId));
    }

    // 店铺年度每月营业额走势图
    @RequestMapping(value = "/getYearShopSaleCurve")
    public ResultVO getYearShopSaleCurve(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                         @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getYearShopSaleCurve(date, shopId));
    }

    // 店铺年度每月销售详情
    @RequestMapping(value = "/getYearShopSaleDetail")
    public ResultVO getYearShopSaleDetail(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                          @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getYearShopSaleDetail(date, shopId));
    }

    // 年度员工业绩排行榜
    @RequestMapping(value = "/getYearStaffSaleTop")
    public ResultVO getYearStaffSaleTop(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                        @NotBlank(message = "shopId不能为空") String shopId) {
        return ResultVO.success(rptAdminService.getYearStaffSaleTop(date, shopId));
    }

    // 员工每周销售量TOP
    @RequestMapping(value = "/getStaffHotGoodsNumTop")
    public ResultVO getStaffHotGoodsNumTop(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                           @NotBlank(message = "staffId不能为空") String staffId) {
        return ResultVO.success(rptAdminService.getStaffHotGoodsNumTop(date, staffId));
    }

    // 员工每周销售额TOP
    @RequestMapping(value = "/getStaffHotGoodsMoneyTop")
    public ResultVO getStaffHotGoodsMoneyTop(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                             @NotBlank(message = "staffId不能为空") String staffId) {
        return ResultVO.success(rptAdminService.getStaffHotGoodsMoneyTop(date, staffId));
    }

    // 店铺每周单个店员销售统计（用于员工报表）
    @RequestMapping(value = "/getStaffWeekSale")
    public ResultVO getStaffWeekSale(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                     @NotBlank(message = "staffId不能为空") String staffId) {
        return ResultVO.success(rptAdminService.getStaffWeekSale(date, staffId));
    }

    // 店铺每周热销商品榜(员工角度)
    @RequestMapping(value = "/getStaffWeekHotGoodsTop")
    public ResultVO getStaffWeekHotGoodsTop(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                            @NotBlank(message = "staffId不能为空") String staffId) {
        return ResultVO.success(rptAdminService.getStaffWeekHotGoodsTop(date, staffId));
    }

    // 单一员工每日销售概览
    @RequestMapping(value = "/getStaffDaySale")
    public ResultVO getStaffDaySale(@NotNull(message = "date不能为空") @PastOrPresent(message = "日期超过当前时间或格式错误") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date,
                                    @NotBlank(message = "staffId不能为空") String staffId) {
        return ResultVO.success(rptAdminService.getStaffDaySale(date, staffId));
    }
}
