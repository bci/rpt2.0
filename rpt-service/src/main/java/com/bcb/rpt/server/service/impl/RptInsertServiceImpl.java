package com.bcb.rpt.server.service.impl;

import com.bcb.rpt.server.dto.RptDragonTigerContentDTO;
import com.bcb.rpt.server.entity.RptDragonTigerContent;
import com.bcb.rpt.server.repository.RptDragonTigerContentJpa;
import com.bcb.rpt.server.service.RptInsertService;
import com.bcb.rpt.server.util.RtpIdUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Optional;
@Transactional(rollbackFor = Exception.class)
@Service
public class RptInsertServiceImpl implements RptInsertService {
    @Resource
    RtpIdUtil rtpIdUtil;
    @Resource
    RptDragonTigerContentJpa rptDragonTigerContentJpa;

    @Override
    public String insertExitRecollection(RptDragonTigerContentDTO param) {
        RptDragonTigerContent exists = rptDragonTigerContentJpa.findByStaffIdAndMonthAndType(param.getStaffId(), param.getMonth(), param.getType());
        if (exists != null) {
            exists.setContent(param.getContent());
            return exists.getId();
        }
        // 创建实体类
        RptDragonTigerContent dragonTigerContent = new RptDragonTigerContent();
        BeanUtils.copyProperties(param, dragonTigerContent);
        //赋值
        dragonTigerContent.setCreateTime(LocalDateTime.now());
        dragonTigerContent.setUpdateTime(LocalDateTime.now());
        dragonTigerContent.setContent(param.getContent());
        dragonTigerContent.setType(param.getType());
        dragonTigerContent.setMonth(param.getMonth());
        dragonTigerContent.setStaffId(param.getStaffId());
        dragonTigerContent.setId(rtpIdUtil.createId("DTC"));
        // 保存
        dragonTigerContent = rptDragonTigerContentJpa.save(dragonTigerContent);
        return dragonTigerContent.getId();
    }

    // 删除
    @Override
    public void delete(String id) {
        Optional<RptDragonTigerContent> byId = rptDragonTigerContentJpa.findById(id);
        if (byId.isPresent()) {
            rptDragonTigerContentJpa.delete(byId.get());
        }
    }


}
