package com.bcb.rpt.server.repository;

import com.alibaba.druid.proxy.jdbc.JdbcParameter;
import com.bcb.rpt.server.entity.OdrOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;

public interface OdrOrderJpa extends JpaRepository<OdrOrder, String> {
//    当天开了单的商户
    @Query(nativeQuery = true, value = "SELECT ifnull(count(*), 0) saleMerNum from (SELECT count(*) from ((odr_usr_salesman a " +
            "left join usr_merchant_staff b on a.staff_id=b.id) left join mct_shop c on b.shop_id=c.id) " +
            "left join mct_merchant d on c.merchant_id=d.id where DateDIff(now(),a.update_time) = 0 " +
            "GROUP BY c.merchant_id) as a")
    long getTodayBillingNum();
}
