package com.bcb.rpt.server.exception;

import com.bcb.rpt.common.enums.ResultEnum;
import lombok.Getter;

/**
 * @author chiangtaol
 * @date 2020-04-24
 * @describe 自定义异常
 */
@Getter
public class WarnException extends RuntimeException {

    /** 异常状态码*/
    private String code;


    /**
     * @description 异常构造方法
     * @author chiangtaol
     * @param code 异常码,返回状态码
     * @param msg   提示信息
     * @return
     */
    public WarnException(String code, String msg){
        super(msg);
        this.code = code;
    }


    /**
     * @description 异常构造方法
     * @author chiangtaol
     * @param resultEnum 返回码与返回信息枚举类
     * @return
     */
    public WarnException(ResultEnum resultEnum){
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }

    public WarnException(ResultEnum resultEnum, String supportMsg){
        super(String.format("%s, Support Msg:%s", resultEnum.getMsg(), supportMsg));
        this.code = resultEnum.getCode();
    }
}
