package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptDragonTigerContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;


public interface RptDragonTigerContentJpa extends JpaRepository<RptDragonTigerContent, String>,
        JpaSpecificationExecutor<RptDragonTigerContent> {
    RptDragonTigerContent findByStaffIdAndMonthAndType(String staffId, String month, Integer type);
}
