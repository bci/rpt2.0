package com.bcb.rpt.server.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@ToString
public class DayShopSaleTotalDTO {
    private Integer sale_num;
    private Long sale_money;
    private Integer bill_num;
    private Integer refund_num;
    private Long refund_money;
    private Integer pre_num;
    private Long pre_money;

}
