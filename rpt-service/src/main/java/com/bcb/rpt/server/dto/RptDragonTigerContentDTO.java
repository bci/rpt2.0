package com.bcb.rpt.server.dto;


import lombok.Data;
import lombok.Getter;

@Data
public class RptDragonTigerContentDTO {
    /**
     * 用户id
     */
    private String staffId;

    /**
     * 该条数据的归属时间
     */
    private String month;

    /**
     * 类型 1.协作 2.销售 3.利润 4.跨部门销售 5.满意度（一期没有）
     */
    private Integer type;

    /**
     * 感言内容
     */
    private String content;
}
