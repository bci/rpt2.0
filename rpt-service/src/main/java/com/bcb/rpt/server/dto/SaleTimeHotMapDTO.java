package com.bcb.rpt.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SaleTimeHotMapDTO {
    private String shop_id;
    private Integer total_goods_quantity;
    private Long total_amount;
    private String hour;
}
