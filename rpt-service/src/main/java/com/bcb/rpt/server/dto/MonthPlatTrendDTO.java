package com.bcb.rpt.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MonthPlatTrendDTO {
    private Long total_moneyLong;
    private String total_money;
    private Integer total_merchant;
    private String day;

}
