package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptPlatformDailySummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface RptPlatformDailySummaryJpa extends JpaRepository<RptPlatformDailySummary, String> {
    Optional<RptPlatformDailySummary> findByDate(LocalDate date);

//  平台月度成交额
    @Query(nativeQuery = true, value = "SELECT " +
            "sum(total_money) month_total_money," +
            "date_format(date,'%Y-%m') month " +
            "FROM rpt_platform_daily_summary " +
            "where date_format(date,'%Y') = date_format(now(),'%Y') " +
            "group by month " +
            " order by month" )
    List<Map<String,Object>> getMonthPlatSale(@Param("date") LocalDate date);

//  平台收入支付情况
    @Query(nativeQuery = true, value = "SELECT " +
            "sum(weixin_rebate) wei_xin," +
            "sum(ali_pay_rebate) ali," +
            "sum(service_income) service_income," +
            "sum(platfrom_pay) month_pay," +
            "date_format(date,'%Y-%m') month " +
            "FROM rpt_platform_daily_summary " +
            "where date_format(date,'%Y') = date_format(now(),'%Y') " +
            "group by month " +
            "order by month" )
    List<Map<String,Object>> getPlatIncomePay(@Param("date") LocalDate date);

//  月度平台总览
    @Query(nativeQuery = true, value = "SELECT " +
            "ifnull(sum(total_money), 0) month_total_money," +
            "ifnull(sum(add_merchant), 0) month_add_merchant," +
            "ifnull(sum(weixin_rebate), 0) wei_xin," +
            "ifnull(sum(ali_pay_rebate), 0) ali," +
            "ifnull(sum(service_income), 0) service_income," +
            "ifnull(sum(platfrom_pay), 0) month_pay" +
            " from rpt_platform_daily_summary " +
            "where date_format(create_time,'%Y-%m') = date_format(:date,'%Y-%m')")
    Map<String, Object> getMonthPlatList(@Param("date") LocalDate date);

//    月度商家统计
    @Query(nativeQuery = true, value = "select ifnull(total_merchant, 0) total_merchant from rpt_platform_daily_summary where " +
            "date = :date")
    Integer getMonthMctMerchant(@Param("date") LocalDate date);

    //月度平台成交额/商户数走势图/每日详情表
    @Query(nativeQuery = true, value = "SELECT " +
            "total_money total_money," +
            "total_merchant total_merchant," +
            "date_format(date,'%m-%d') day" +
            " FROM rpt_platform_daily_summary " +
            "where date_format(date,'%Y-%m') = date_format(:date,'%Y-%m') " +
            "order by day asc" )
    List<Map<String,Object>> getMonthPlatTrend(@Param("date") LocalDate date);
}
