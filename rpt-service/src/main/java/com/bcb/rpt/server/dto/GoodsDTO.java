package com.bcb.rpt.server.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class GoodsDTO {

    private String id;

    private String goodsName;

    private String goodsUrl;

    private BigDecimal price;
//    private Long price;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Integer isDeleted;

}
