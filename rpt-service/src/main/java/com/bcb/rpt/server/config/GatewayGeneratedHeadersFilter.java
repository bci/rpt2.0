package com.bcb.rpt.server.config;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author chiangtaol
 * @date 2020-04-29
 * @describe 将Gateway产生的Header参数设置到form中
 */
@Slf4j
@Component
public class GatewayGeneratedHeadersFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(new GatewayGeneratedHeadersWrapper((HttpServletRequest) servletRequest), servletResponse);
    }
}
