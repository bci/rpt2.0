package com.bcb.rpt.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MonthPlatListDTO {
    private Long month_total_moneyLong;
    private String month_total_money;
    private Integer month_add_merchant;
    private Long plat_month_incomeLong;
    private String plat_month_income;
    private Long month_payLong;
    private String month_pay;
    private Long avg_month_moneyLong;
    private String avg_month_money;
    private Integer total_merchant;
    private Long wei_xin;
    private Long ali;
    private Long service_income;
}
