package com.bcb.rpt.server.repository;
import com.bcb.rpt.server.entity.RptPlatformCategoryPercent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RptPlatformCategoryInfoJpa extends JpaRepository<RptPlatformCategoryPercent, String> {
    List<RptPlatformCategoryPercent> findAllByDate(LocalDate date);
}
