package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.OdrProcPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Map;

public interface OdrProcPaymentJpa extends JpaRepository<OdrProcPayment, String> {


//  当天实时成交额
    @Query(nativeQuery=true,value="select sum(total_price) from odr_proc_payment where DateDIff(now(),update_time) = 0")
    Long getRealTimeAmount(@Param("nowDate") LocalDate nowDate);
// 情况销售汇总
    @Query(nativeQuery=true, value="select " +
            "ifnull(sum(total_price), 0) totalAmount," +
            "count(*) totalCount," +
            "count(case when DateDIff(:update_time,update_time) = 0 then 1 end) nowCount," +
            "count(case when DateDIff(:update_time,update_time) = 1 then 1 end) yesterdayCount" +
            " from odr_proc_payment")
    Map<String,Object> getPlatFormSaleStat(@Param("update_time") LocalDate update_time);


}
