package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.UsrUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsrUserJpa extends JpaRepository<UsrUser, String> {
}
