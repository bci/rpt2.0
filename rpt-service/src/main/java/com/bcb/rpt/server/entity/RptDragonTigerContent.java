package com.bcb.rpt.server.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: app龙虎榜评论表 ( rpt_dragon_tiger_connent )
 * @date 2020-07-27 09:59
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class RptDragonTigerContent {
    /**
     * id
     */
    @Id
    private String id;

    /**
     * 员工id
     */
    private String staffId;

    /**
     * 该条数据的归属时间
     */
    private String month;

    /**
     * 类型 1.协作 2.销售 3.利润 4.跨部门销售 5.满意度（一期没有）
     */
    private Integer type;

    /**
     * 感言内容
     */
    private String content;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;

}
