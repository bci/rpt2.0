package com.bcb.rpt.server.dto;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
/**
 * @author Mr.Li
 * @description: 平台商户情况统计
 * @date 2020-07-07 16:09
 */

@Getter
@Setter
@ToString
public class PlatFormMctChantStatDTO {
    private Long totalMcrChant;
    private Long nowAddMerChant;
    private Long nowDleMerChant;
    private BigDecimal churnRate;
    private BigDecimal vitality;
}
