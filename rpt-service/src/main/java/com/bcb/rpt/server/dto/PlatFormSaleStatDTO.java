package com.bcb.rpt.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * @author Mr.Li
 * @description: 平台销售情况统计
 * @date 2020-07-07 16:09
 */

@Getter
@Setter
@ToString
public class PlatFormSaleStatDTO {
    private Long totalAmountLong;
    private String totalAmount;
    private Long totalCount;
    private Long nowCount;
    private Long yesterdayCount;
    private BigDecimal rate;
}
