package com.bcb.rpt.server.handler;

import com.bcb.commom.vo.ResultVO;
import com.bcb.rpt.common.enums.ResultEnum;
import com.bcb.rpt.server.exception.WarnException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

/**
 * @author chiangtaol
 * @date 2020-04-24
 * @describe 自定义异常处理
 */
@ControllerAdvice
@Slf4j
public class WarnExceptionHandler {

    @ExceptionHandler(WarnException.class)
    @ResponseBody
    public ResultVO exceptionHandler(WarnException e){
        return ResultVO.error(e.getCode(),e.getMessage());
    }

    /**
     * 参数校验异常拦截
     *      对象参数接收请求体： MethodArgumentNotValidException
     *      请求参数绑定到对象参数上： BindException
     *      普通参数： ConstraintViolationException
     *      必填参数没传： ServletRequestBindingException
     *          必填请求参数缺失：MissingServletRequestParameterException
     *          路径参数缺失：MissingPathVariableException
     * @param e 异常
     * @return 统一返回结果
     */
    @ExceptionHandler({ConstraintViolationException.class,
            MethodArgumentNotValidException.class,
            ServletRequestBindingException.class,
            BindException.class})
    @ResponseBody
    public ResultVO handleValidationException(Exception e) {

        String msg = null;

        //参数无效异常
        if (e instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException t = (MethodArgumentNotValidException) e;
            msg = getBindingResultMsg(t.getBindingResult());
        }

        //参数绑定校验异常
        if (e instanceof BindException) {
            BindException t = (BindException) e;
            msg = getBindingResultMsg(t.getBindingResult());
        }

        //参数约束违规异常
        if (e instanceof ConstraintViolationException) {
            ConstraintViolationException t = (ConstraintViolationException) e;
            msg = t.getConstraintViolations().stream()
                    .map(ConstraintViolation::getMessage)
                    .collect(Collectors.joining(","));
        }

        //请求缺少Servlet参数异常
        if (e instanceof MissingServletRequestParameterException) {
            MissingServletRequestParameterException t = (MissingServletRequestParameterException) e;
            msg = t.getParameterName() + " 不能为空";
        }

        if (e instanceof MissingPathVariableException) {
            MissingPathVariableException t = (MissingPathVariableException) e;
            msg = t.getVariableName() + " 不能为空";
        }

        if (msg == null) {
            msg = "参数未知错误";
        }

        log.warn("参数校验不通过,msg: {}", msg);

        //拼装统一返回
        ResultVO resultVO = new ResultVO();
        resultVO.setCode(ResultEnum.PARAM_ERROR_FORMAT.getCode());
        resultVO.setMsg(msg);
        resultVO.setResult(null);
        resultVO.setSuccess(false);

        return resultVO;
    }


    /**
     * 获取错误消息
     * @param result 校验结果
     * @return 错误消息
     */
    private String getBindingResultMsg(BindingResult result) {
        return result.getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(","));
    }

}
