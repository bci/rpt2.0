package com.bcb.rpt.server.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: 商户员工表 ( usr_merchant_staff )
 * @date 2020-07-07 16:09
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class UsrMerchantStaff {

    /**
     * id
     */
    @Id
    private String id;

    /**
     * 店铺id
     */
    private String shopId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 部门id
     */
    private String departmentId;

    /**
     * 备注
     */
    private String comment;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 删除标识
     */
    private Integer isDelete;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;
}
