package com.bcb.rpt.server.repository;
import com.bcb.rpt.server.entity.MctMerchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface MctMerchantJpa extends JpaRepository<MctMerchant, String> {
//    入驻商家统计
    @Query(nativeQuery = true, value = "select count(*) from mct_merchant where state=1")
    Long getMctMerchant();
//    商户情况统计
    @Query(nativeQuery=true, value="select " +
        "count(case when is_delete=0 then 1 end) totalMcrChant," +
        "count(case when DateDIff(:nowDate,create_time) = 0 and is_delete=0 then 1 end) nowAddMerChant," +
        "count(case when DateDIff(:nowDate,update_time) = 0 and is_delete>0 then 1 end) nowDleMerChant" +
        " from mct_merchant " +
        "where state=1")
    Map<String,Object> getPlatFormMctChant(@Param("nowDate")LocalDate nowDate);

//  商户当天成交额排行
    @Query(nativeQuery = true, value = "select " +
            "sum( a.total_price) todayAmount," +
            "e.id merchant_id," +
            "e.merchant_name merchant_name " +
            "from odr_proc_payment a left join odr_usr_salesman b on a.order_id=b.order_id " +
            "left join usr_merchant_staff c on b.staff_id=c.id " +
            "left join mct_shop d on c.shop_id=d.id " +
            "left join mct_merchant e on d.merchant_id=e.id " +
            "where DATEDIFF(:nowDate, a.update_time)=0 " +
            "group by e.id,e.merchant_name " +
            "order by todayAmount desc limit 7")
    List<Map<String,Object>> getMerchantSaleTop(@Param("nowDate")LocalDate nowDate);

    // 平台月度商户成交额排行
    @Query(nativeQuery = true, value = "select " +
            "ifnull(sum( a.total_price), 0) todayAmount," +
            "ifnull(count(*), 0) odrNum," +
            "e.id merchant_id," +
            "e.merchant_name merchant_name " +
            "from odr_proc_payment a left join odr_usr_salesman b on a.order_id=b.order_id " +
            "left join usr_merchant_staff c on b.staff_id=c.id " +
            "left join mct_shop d on c.shop_id=d.id " +
            "left join mct_merchant e on d.merchant_id=e.id " +
            "where date_format(a.update_time, '%Y-%m')=:date " +
            "group by e.id,e.merchant_name " +
            "order by todayAmount desc")
    List<Map<String, Object>> getMonthMerchantSaleTop(@Param("date") String date);

    // 计算平台月度商户成交额同比
    @Query(nativeQuery = true, value = "select " +
            "sum( a.total_price) hisTodayAmount," +
            "e.id merchant_id " +
            "from odr_proc_payment a left join odr_usr_salesman b on a.order_id=b.order_id " +
            "left join usr_merchant_staff c on b.staff_id=c.id " +
            "left join mct_shop d on c.shop_id=d.id " +
            "left join mct_merchant e on d.merchant_id=e.id " +
            "where date_format(a.update_time, '%Y-%m')= :date  and e.id in :merchantIds " +
            "group by e.id")
    List<Map<String, Object>> getHisMonthMerchantSale(@Param("date") String date, Collection<String> merchantIds);

}


