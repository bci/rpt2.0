package com.bcb.rpt.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DayStaffSaleDTO {
    private String name;
    private String staff_id;
    private String shop_id;
    private Integer sale_num;
    private Long sale_money;
}
