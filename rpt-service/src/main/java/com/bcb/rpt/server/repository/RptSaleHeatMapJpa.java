package com.bcb.rpt.server.repository;
import com.bcb.rpt.server.entity.RptSaleHeatMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface RptSaleHeatMapJpa extends JpaRepository<RptSaleHeatMap, String> {
    Optional<RptSaleHeatMap> findByDateAndShopId(LocalDateTime date, String shopId);

    // 店铺销售/时间热力图
    @Query(nativeQuery = true, value = "select " +
    "shop_id shop_id," +
    "total_amount total_amount," +
    "total_goods_quantity total_goods_quantity, " +
    "date_format(create_time, '%H:00') hour " +
    " FROM rpt_sale_heat_map " +
    "where date_format(date, '%m-%d') = date_format(:date, '%m-%d') and shop_id = :shopId" +
    " order by hour asc ")
    List<Map<String, Object>> getSaleTimeHotMap(@Param("date")LocalDate date, @Param("shopId")String shopId);


}
