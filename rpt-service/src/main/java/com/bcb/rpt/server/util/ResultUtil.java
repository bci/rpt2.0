package com.bcb.rpt.server.util;

import com.bcb.commom.vo.ResultVO;
import com.bcb.rpt.common.enums.ResultEnum;

/**
 * @author DaMai
 * @description: 返回结果工具类
 * @date 2020/4/20 18:06
 */
public class ResultUtil {

    /**
     * 无参成功
     * @return 统一返回结果
     */
    public static ResultVO success() {
        ResultVO resultVo = new ResultVO();
        resultVo.setCode("000000");
        resultVo.setMsg("请求成功");
        resultVo.setSuccess(Boolean.TRUE);
        return resultVo;
    }

    /**
     * 有参成功
     * @param object 自定实体
     * @return 统一返回结果
     */
    public static ResultVO success(Object object) {
        ResultVO resultVo = new ResultVO();
        resultVo.setCode("000000");
        resultVo.setMsg("请求成功");
        resultVo.setSuccess(Boolean.TRUE);
        resultVo.setResult(object);
        return resultVo;
    }

    /**
     * 有参成功
     * @param msg 自定消息
     * @param object 自定实体
     * @return 统一返回结果
     */
    public static ResultVO success(String msg, Object object) {
        ResultVO resultVo = new ResultVO();
        resultVo.setCode("000000");
        resultVo.setMsg(msg);
        resultVo.setResult(object);
        resultVo.setSuccess(Boolean.TRUE);
        return resultVo;
    }

    /**
     * 有参失败
     * @param resultEnum 错误码及释义
     * @return 统一返回结果
     */
    public static ResultVO error(ResultEnum resultEnum) {
        ResultVO resultVo = new ResultVO();
        resultVo.setCode(ResultEnum.ERROR.getCode());
        resultVo.setMsg(ResultEnum.ERROR.getMsg());
        resultVo.setSuccess(Boolean.FALSE);
        return resultVo;
    }

    /**
     * 有参失败
     * @param resultEnum 错误码及释义
     * @return 统一返回结果
     */
    public static ResultVO error(ResultEnum resultEnum, Object object) {
        ResultVO resultVo = new ResultVO();
        resultVo.setCode(ResultEnum.ERROR.getCode());
        resultVo.setMsg(ResultEnum.ERROR.getMsg());
        resultVo.setResult(object);
        resultVo.setSuccess(Boolean.FALSE);
        return resultVo;
    }

    /**
     * 有参失败
     * @param errCode 错误码
     * @param errMsg 错误释义
     * @return 统一返回结果
     */
    public static ResultVO error(String errCode, String errMsg) {
        ResultVO resultVo = new ResultVO();
        resultVo.setCode(errCode);
        resultVo.setMsg(errMsg);
        resultVo.setSuccess(Boolean.FALSE);
        return resultVo;
    }

    /**
     * 有参失败
     * @param errCode 错误码
     * @param errMsg 错误描述
     * @param object 错误详情
     * @return 统一返回结果
     */
    public static ResultVO error(String errCode, String errMsg, Object object) {
        ResultVO<Object> resultVo = new ResultVO<>();
        resultVo.setCode(errCode);
        resultVo.setMsg(errMsg);
        resultVo.setResult(object);
        resultVo.setSuccess(Boolean.FALSE);
        return resultVo;
    }

}
