package com.bcb.rpt.server.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;

import java.lang.Long;
import javax.persistence.Entity;

import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.Id;


/**
 * @author Mr.Ma
 * @description: 平台每日交易信息统计表 ( rpt_platform_daily_summary )
 * @date 2020-05-15 12:08
 */
@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class RptPlatformDailySummary {

    /**
     * id
     */
    @Id
    private String id;


    /**
     * 数据统计时间(精确到天即可)
     */
    @CreatedDate    /** 自动赋值创建时间*/
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    @LastModifiedDate    /** 该注解自动修改更新时间  配合@EntityListeners使用*/
    private LocalDateTime updateTime;

    /**
     * 平台每日总成交量
     */
    private Integer totalNum;

    /**
     * 平台每日总成交额
     */
    private Long totalMoney;

    /**
     * 平台截至今日总入驻商户数
     */
    private Integer totalMerchant;

    /**
     * 平台截至今日总店铺数
     */
    private Integer totalShop;

    /**
     * 每日微信服务商返利
     */
    private Long weixinRebate;

    /**
     * 每日阿里服务商返利
     */
    private Long aliPayRebate;

    /**
     * 平台每日增值服务收入汇总
     */
    private Long serviceIncome;

    /**
     * 平台每日支付汇总
     */
    private Long platfromPay;

    /**
     * 版本控制锁（乐观锁），默认值0
     */
    private Integer version;

    /**
     * 平台截止今日新增的商户数
     */
    private Integer addMerchant;

    /**
     * 具体统计数据的归属时间
     */
    private LocalDate date;

}