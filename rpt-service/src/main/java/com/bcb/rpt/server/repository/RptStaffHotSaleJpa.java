package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptStaffHotSale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface RptStaffHotSaleJpa extends JpaRepository<RptStaffHotSale, String> {
    List<RptStaffHotSale> findAllByDateAndStaffId(LocalDate date,String staffId);

    // 员工每周销售量TOP
    @Query(nativeQuery = true, value = "select " +
    "goods_name goodsName," +
    "sum(total_num) totalNum " +
    "from rpt_staff_hot_sale " +
    "where date between :startDate and :endDate and staff_id = :staffId" +
    " group by goodsName " +
    "order by totalNum desc ")
    List<Map<String, Object>> getStaffHotGoodsNumTop(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                                  @Param("staffId")String staffId);

    // 员工每周销售额TOP
    @Query(nativeQuery = true, value = "select " +
            "goods_name goodsName," +
            "sum(total_amount) totalAmount " +
            "from rpt_staff_hot_sale " +
            "where date between :startDate and :endDate and staff_id = :staffId" +
            " group by goodsName " +
            "order by totalAmount desc ")
    List<Map<String, Object>> getStaffHotGoodsMoneyTop(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                                  @Param("staffId")String staffId);
    
}
