package com.bcb.rpt.server.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: 店铺销售热力图（按时间）表 ( rpt_sale_heat_map )
 * @date 2020-07-07 16:29
 */
@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class RptSaleHeatMap {

    /**
     * id
     */
    @Id
    private String id;

    /**
     * 关联店铺表id
     */
    private String shopId;

    /**
     * 该店铺每小时的销量
     */
    private Integer totalGoodsQuantity;

    /**
     * 该店铺每小时的销售额
     */
    private Long totalAmount;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 版本控制锁（乐观锁）,默认值0
     */
    private Integer version;

    /**
     * 该条数据的归属时间
     */
    private LocalDateTime date;
}
