package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptStaffSaleInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;


public interface RptStaffSaleInfoJpa extends JpaRepository<RptStaffSaleInfo, String> {
        List<RptStaffSaleInfo>findAllByDateAndShopId(LocalDate date,String shopId);

        // 店铺员工每天销售情况统计
        @Query(nativeQuery = true, value = "SELECT " +
                "name name," +
                "staff_id staff_id," +
                "shop_id shop_id," +
                "sale_num sale_num," +
                "sale_money sale_money " +
                "FROM rpt_staff_sale_info " +
                "where date = :date and shop_id = :shopId" +
                " order by sale_money desc ")
        List<Map<String, Object>> getDayStaffSale(@Param("date")LocalDate date, @Param("shopId")String shopId);

        // 店铺每周销售统计 单查姓名
        @Query(nativeQuery = true, value = "select " +
        "name name " +
        "from rpt_staff_sale_info " +
        "where date between :startDate and :endDate and shop_id = :shopId" +
        " group by name order by sum(sale_money) desc limit 1 ")
        String getWeekStaffName(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                             @Param("shopId")String shopId);

        // 店铺每周店员销售统计
        @Query(nativeQuery = true, value = "select " +
                "name name, " +
                "sum(sale_money) sale_money " +
                "from rpt_staff_sale_info " +
                "where date between :startDate and :endDate and shop_id = :shopId" +
                " group by name order by sum(sale_money) desc")
        List<Map<String, Object>> getWeekStaffNameNum(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                @Param("shopId")String shopId);

        // 每月员工业绩排行榜
        @Query(nativeQuery = true, value = "select " +
        "name name," +
        "sum(sale_money) saleMoney " +
        "from rpt_staff_sale_info " +
        "where date_format(date, '%Y-%m') = date_format(:date, '%Y-%m') and shop_id = :shopId " +
                "group by name" +
        " order by saleMoney desc ")
        List<Map<String, Object>> getMonthStaffSaleTop(@Param("date")LocalDate date, @Param("shopId")String shopId);

        // 年度员工业绩排行榜
        @Query(nativeQuery = true, value = "select " +
                "name name," +
                "sum(sale_money) saleMoney " +
                "from rpt_staff_sale_info " +
                "where date_format(date, '%Y') = date_format(:date, '%Y') and shop_id = :shopId " +
                "group by name" +
                " order by saleMoney desc ")
        List<Map<String, Object>> getYearStaffSaleTop(@Param("date")LocalDate date, @Param("shopId")String shopId);

        // 店铺每周单个店员销售统计（用于员工报表）
        @Query(nativeQuery = true, value = "select " +
        "sum(sale_num) saleNum," +
        "sum(sale_money) saleMoney," +
        "name name " +
        "from rpt_staff_sale_info " +
        "where date between :startDate and :endDate and staff_id = :staffId" +
        " group by name")
        Map<String, Object> getStaffWeekSale(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                             @Param("staffId")String staffId);

        // 单一员工每日销售概览
        @Query(nativeQuery = true, value = "select " +
        "sale_num saleNum," +
        "sale_money saleMoney," +
        "date_format(date, '%m-%d') day " +
        "from rpt_staff_sale_info " +
        "where date between :startDate and :endDate and staff_id = :staffId " +
        "order by day asc ")
        List<Map<String, Object>> getStaffDaySale(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                                  @Param("staffId")String staffId);

}
