package com.bcb.rpt.server.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

/**
 * @author Mr.Li
 * @description: 商户公司信息表 ( mct_merchant )
 * @date 2020-07-07 16:09
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class MctMerchant {

    /**
     * id
     */
    @Id
    private String id;

    /**
     * 公司名称
     */
    private String merchantName;
    /**
     * 营业执照号，统一社会代码
     */
    private String businessLicenseNumber;
    /**
     * 证件类型，是否3证合一，1-是，0否
     */
    private Integer businessLicenseType;
    /**
     * 营业执照图片
     */
    private String businessLicenseAddress;
    /**
     * 营业执照有效期
     */
    private String businessLicenseTerm;
    /**
     * 经营详细地址
     */
    private String address;
    /**
     * 公司图标
     */
    private String logo;
    /**
     * 法人或经营者身份证正面
     */
    private String corporationCertPositiveUrl;
    /**
     * 法人或经营者身份证反面
     */
    private String corporationCertBackUrl;
    /**
     * 商户类型，1-企业，2-个体户
     */
    private Integer merchantType;
    /**
     * 证件类型，1-身份证、2-港澳台通行证、3-护照
     */
    private Integer certType;
    /**
     * 法人或经营者姓名
     */
    private String corporationName;
    /**
     * 法人证件有效期
     */
    private String certTerm;

    /** 法人证件号码*/
    private String certNumber;

    /** 联系电话*/
    private String contactPhone;

    /** 联系人*/
    private String contactName;

    /** 申请人用户id*/
    private String applyUserId;

    
}
