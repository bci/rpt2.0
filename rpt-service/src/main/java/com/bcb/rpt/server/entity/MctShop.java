package com.bcb.rpt.server.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: 商户门店表 ( mct_shop )
 * @date 2020-07-07 16:09
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)

public class MctShop {

    /**
     * id
     */
    @Id
    private String id;

    /**
     * 详细地址
     */
    private String address;
    /**
     * 营业执照图片
     */
    private String businessLicenseAddress;
    /**
     * 营业执照号
     */
    private String businessLicenseNumber;
    /**
     * 联系方式
     */
    private String contactPhone;
    /**
     * 商户id
     */
    private String merchantId;
    /**
     * 地区编码
     */
    private Integer areaCode;
    /**
     * 联系人
     */
    private String contactName;
    /**
     * 结束营业时间
     */
    private String closingTime;
    /**
     * 开始营业时间
     */
    private String openingTime;
    /**
     * 店铺名称
     */
    private String name;
    /**
     * 官方授权许可证书
     */
    private String officialLicenseImageUrl;
    /**
     * 店铺门头
     */
    private String shopDoorImageUrl;
    /**
     * 店铺简介
     */
    private String tips;
    /**
     * 经度
     */
    private BigDecimal longitude;
    /**
     * 纬度
     */
    private BigDecimal latitude;

    /** 微信收款账号*/
    private String wxAccountId;

    /** 支付宝收款账号*/
    private String aliAccountId;

    /** 申请人id*/
    private String userId;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;
}
