package com.bcb.rpt.server.service.impl;

import com.bcb.commom.util.MoneyUtils;
import com.bcb.rpt.common.enums.DragonTigerStatisticsTypeEnum;
import com.bcb.rpt.common.enums.ResultEnum;
import com.bcb.rpt.common.form.AppStaffResultInfoForm;
import com.bcb.rpt.common.vo.*;
import com.bcb.rpt.server.dto.*;
import com.bcb.rpt.server.entity.*;
import com.bcb.rpt.server.exception.WarnException;
import com.bcb.rpt.server.repository.*;
import com.bcb.rpt.server.service.RptAdminService;
import com.bcb.rpt.server.service.impl.helper.AppStaffResultInfoHelper;
import com.bcb.rpt.server.service.impl.helper.DragonTigerStatisticsHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.bcb.rpt.common.enums.DragonTigerStatisticsTypeEnum.*;

/**
 * @author: Mr.Yuan
 * @date: 2020-05-11
 * @description: 菜单管理实现类
 */
@Service
@Slf4j
public class RptAdminServiceImpl implements RptAdminService {
    @Resource
    RptPlatformDailySummaryJpa rptPlatformDailySummaryJpa;

    @Resource
    RptPlatformCategoryInfoJpa rptPlatformCategoryInfoJpa;

    @Resource
    RptShopSummaryJpa rptshopSummaryJpa;

    @Resource
    RptGoodsSaleNumJpa rptGoodsSaleNumJpa;

    @Resource
    RptHighStockWarningJpa rptHighStockWarningJpa;

    @Resource
    RptLowStockWarningJpa rptLowStockWarningJpa;

    @Resource
    RptSaleHeatMapJpa rptSaleHeatMapJpa;

    @Resource
    RptStaffHotSaleJpa rptStaffHotSaleJpa;

    @Resource
    RptStaffSaleInfoJpa rptStaffSaleInfoJpa;

    @Resource
    OdrProcPaymentJpa odrProcPaymentJpa;

    @Resource
    MctMerchantJpa mctMerchantJpa;

    @Resource
    OdrOrderJpa odrOrderJpa;

    @Resource
    RptDragonTigerMonthJpa rptDragonTigerMonthJpa;

    @Resource
    RptDragonTigerDayJpa rptDragonTigerDayJpa;

    @Resource
    RptDragonTigerContentJpa rptDragonTigerContentJpa;

    @Resource
    UsrMerchantStaffJpa usrMerchantStaffJpa;

    @Resource
    UsrUserJpa usrUserJpa;

    private static Object getValue(Object object, String attrName) {
        PropertyDescriptor propertyDescriptor = BeanUtils.getPropertyDescriptor(object.getClass(), attrName);
        if (propertyDescriptor == null || propertyDescriptor.getReadMethod() == null
                || !propertyDescriptor.getReadMethod().canAccess(object)) {
            return null;
        }
        try {
            return propertyDescriptor.getReadMethod().invoke(object);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param date: 待获取权限的ID
     * @description: 根据权限ID获取权限详细信息
     * @return: com.bcb.user.common.vo.UsrAuthVO
     */
    //  平台每日交易信息统计
    @Override
    public RptPlatformDailySummaryVO getPlatformDailySummary(LocalDate date) {
        Optional<RptPlatformDailySummary> item = rptPlatformDailySummaryJpa.findByDate(date);
        if (item.isPresent()) {
            RptPlatformDailySummaryVO vo = new RptPlatformDailySummaryVO();
            BeanUtils.copyProperties(item.get(), vo);
            return vo;
        } else {
            throw new WarnException(ResultEnum.USR_ERROR_DB_QUERY_RECORD_NOT_EXIST);
        }
    }

    //  记录平台内销售商品所属分类，主要是汇总扇形占比图
    @Override
    public List<RptPlatformCategoryInfoVO> getPlatformCategoryInfo(LocalDate date) {
        LocalDate date1 = date.minusDays(1);
        List<RptPlatformCategoryPercent> all = rptPlatformCategoryInfoJpa.findAllByDate(date1);
        List<RptPlatformCategoryInfoVO> voList = all.stream().map(temp -> {
            RptPlatformCategoryInfoVO vo = po2vo(temp);
            vo.setName(temp.getCategoryName());
            vo.setValue(MoneyUtils.handleMoney(temp.getTotalMoney()));
            return vo;
        }).collect(Collectors.toList());
        return voList;
    }

    //  店铺每日汇总
    @Override
    public RptShopSummaryVO getShopSummary(LocalDate date, String shopId) {
        Optional<RptShopSummary> item = rptshopSummaryJpa.findByDateAndShopId(date, shopId);
        if (item.isPresent()) {
            RptShopSummaryVO vo = new RptShopSummaryVO();
            BeanUtils.copyProperties(item.get(), vo);
            return vo;
        } else {
            throw new WarnException(ResultEnum.USR_ERROR_DB_QUERY_RECORD_NOT_EXIST);
        }
    }

    //  店铺每日各个商品销售量
    @Override
    public List<RptGoodsSaleNumVO> getGoodsSaleNum(LocalDate date, String shopId) {
        List<RptGoodsSaleNum> all = rptGoodsSaleNumJpa.findAllByDateAndShopId(date, shopId);
        List<RptGoodsSaleNumVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    //  高库存商品预警
    @Override
    public RptHighStockWarningVO getHighStockWarning(LocalDate date, String shopId) {
        return getRptHighStockWarningVO(date, shopId);
    }

    private RptHighStockWarningVO getRptHighStockWarningVO(LocalDate date, String shopId) {
        RptHighStockWarningVO result = new RptHighStockWarningVO();
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptHighStockWarningJpa.getHighStockWarning(date, shopId);
        List<RptHighStockWarningVO.Goods> goods = null;
        try {
            goods = mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            goods = new ArrayList<>();
        }
        result.setGoods(goods);
        Integer stockGoodsAllNum = rptHighStockWarningJpa.getStockGoodsAllNum(date, shopId);
        if (stockGoodsAllNum == null || stockGoodsAllNum == 0) {
            result.setPercent(BigDecimal.ZERO);
            result.setHighStockNum(0);
        } else {
            result.setHighStockNum(stockGoodsAllNum);
            BigDecimal a = new BigDecimal(stockGoodsAllNum);
            BigDecimal num = new BigDecimal(result.getGoods().stream()
                    .mapToInt(RptHighStockWarningVO.Goods::getOnStoreNum)
                    .sum());
            BigDecimal pre = num.multiply(new BigDecimal(100)).divide(a, 2, RoundingMode.HALF_UP);
            result.setPercent(pre);
        }
        return result;
    }

    // 周 高库存商品预警
    @Override
    public RptHighStockWarningVO getWeekHighStockWarning(LocalDate date, String shopId) {
        DayOfWeek week = date.getDayOfWeek();
        LocalDate endDate = date.plusDays(7 - week.getValue());
        return getRptHighStockWarningVO(endDate, shopId);
    }

    //  低库存商品预警
    @Override
    public List<RptLowStockWarningVO> getLowStockWarning(LocalDate date, String shopId) {

        List<RptLowStockWarning> all = rptLowStockWarningJpa.findAllByDateAndShopId(date, shopId);
        List<RptLowStockWarningVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    //  店铺销售热力图
    @Override
    public RptSaleHeatMapVO getSaleHeatMap(LocalDateTime date, String shopId) {
        Optional<RptSaleHeatMap> item = rptSaleHeatMapJpa.findByDateAndShopId(date, shopId);
        if (item.isPresent()) {
            RptSaleHeatMapVO vo = new RptSaleHeatMapVO();
            BeanUtils.copyProperties(item.get(), vo);
            return vo;
        } else {
            throw new WarnException(ResultEnum.USR_ERROR_DB_QUERY_RECORD_NOT_EXIST);
        }
    }

    //  员工热卖商品榜统计
    @Override
    public List<RptStaffHotSaleVO> getStaffHotSale(LocalDate date, String staffId) {
        List<RptStaffHotSale> all = rptStaffHotSaleJpa.findAllByDateAndStaffId(date, staffId);
        List<RptStaffHotSaleVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    //  店铺每日员工销售情况统计
    @Override
    public List<RptStaffSaleInfoVO> getStaffSaleInfo(LocalDate date, String shopId) {
        List<RptStaffSaleInfo> all = rptStaffSaleInfoJpa.findAllByDateAndShopId(date, shopId);
        List<RptStaffSaleInfoVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    //  平台每日实时成交额
    @Override
    public long getRealTimeAmount(LocalDate updateTime) {
        Long sum = odrProcPaymentJpa.getRealTimeAmount(updateTime);
        System.out.println(sum);
        return sum == null ? 0 : sum;
    }

    //  平台每日实时入驻商户数量
    @Override
    public long getRealTimeMctMerchant(LocalDate updateTime) {
        Long sum = mctMerchantJpa.getMctMerchant();
        System.out.println(sum);
        return sum == null ? 0 : sum;
    }

    // 平台销售情况汇总
    @Override
    public PlatFormSaleStatDTO getPlatFormSaleStat(LocalDate updateTime) {
        Map<String, Object> result = new HashMap<>(odrProcPaymentJpa.getPlatFormSaleStat(updateTime));
        ObjectMapper mapper = new ObjectMapper();

        try {
            result.put("totalAmountLong", result.get("totalAmount"));
            PlatFormSaleStatDTO stat = mapper.readValue(mapper.writeValueAsString(result), PlatFormSaleStatDTO.class);
            stat.setTotalAmount(MoneyUtils.handleMoney(stat.getTotalAmountLong()));
            BigDecimal now = new BigDecimal(stat.getNowCount());
            BigDecimal yesterday = new BigDecimal(stat.getYesterdayCount());
            if (yesterday.compareTo(BigDecimal.ZERO) > 0) {
                BigDecimal rate = now.subtract(yesterday)
                        .multiply(new BigDecimal(100))
                        .divide(yesterday, 2, RoundingMode.HALF_UP);
                stat.setRate(rate);
            }
            System.out.println(stat);
            return stat;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    //平台商户情况统计
    @Override
    public PlatFormMctChantStatDTO getPlatFormMerchantStat(LocalDate updateTime) {
        Map<String, Object> mer = mctMerchantJpa.getPlatFormMctChant(updateTime);
        long odr = odrOrderJpa.getTodayBillingNum();
        ObjectMapper mapper = new ObjectMapper();
        try {
            PlatFormMctChantStatDTO stat = mapper.readValue(mapper.writeValueAsString(mer), PlatFormMctChantStatDTO.class);
            BigDecimal now = new BigDecimal(stat.getTotalMcrChant());
            BigDecimal dleNow = new BigDecimal(stat.getNowDleMerChant());

            BigDecimal decimal = dleNow.multiply(new BigDecimal(100)).divide(dleNow.add(now), 2, RoundingMode.HALF_UP);
            stat.setChurnRate(decimal);

            BigDecimal order = new BigDecimal(odr);
            stat.setVitality(order.multiply(new BigDecimal(100)).divide(now, 2, RoundingMode.HALF_UP));
            return stat;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    //平台月度成交额统计
    @Override
    public List<MonthPlatSaleVO> getMonthPlatSale(LocalDate date) {
        ObjectMapper mapper = new ObjectMapper();
        List<MonthPlatSaleDTO> all = rptPlatformDailySummaryJpa.getMonthPlatSale(date).stream().map(temp -> {
            try {
                temp = new HashMap<>(temp);
                temp.put("month_total_moneyLong", temp.get("month_total_money"));
                MonthPlatSaleDTO dto = mapper.readValue(mapper.writeValueAsString(temp), MonthPlatSaleDTO.class);
                dto.setMonth_total_money(MoneyUtils.handleMoney(dto.getMonth_total_moneyLong()));
                dto.setMonth(dto.getMonth());
                return dto;

            } catch (JsonProcessingException e) {
                return null;
            }
        }).collect(Collectors.toList());
        List<MonthPlatSaleVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    //平台收入支出情况统计
    @Override
    public List<PlatIncomePayVO> getPlatIncomePay(LocalDate date) {
        ObjectMapper mapper = new ObjectMapper();
        List<PlatIncomePayDTO> all = rptPlatformDailySummaryJpa.getPlatIncomePay(date).stream().map(temp -> {
            try {
                temp = new HashMap<>(temp);
                temp.put("month_payLong", temp.get("month_pay"));
                temp.put("month_incomeLong", temp.get("month_income"));
                PlatIncomePayDTO stat = mapper.readValue(mapper.writeValueAsString(temp), PlatIncomePayDTO.class);
                BigDecimal wei = new BigDecimal(stat.getWei_xin());
                BigDecimal ali = new BigDecimal(stat.getAli());
                BigDecimal sev = new BigDecimal(stat.getService_income());
                BigDecimal month_income = wei.add(ali.add(sev));
                Long sum = month_income.longValue();
                stat.setMonth_income(MoneyUtils.handleMoney(sum));
                stat.setMonth_pay(MoneyUtils.handleMoney(stat.getMonth_payLong()));
                return stat;

            } catch (JsonProcessingException e) {
                return null;
            }
        }).collect(Collectors.toList());
        List<PlatIncomePayVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    // 平台商户当天成交额排行
    @Override
    public List<MerchantSaleTopVO> getMerchantSaleTop(LocalDate updateTime) {
        ObjectMapper mapper = new ObjectMapper();
        List<MerchantSaleTopDTO> all = mctMerchantJpa.getMerchantSaleTop(updateTime).stream().map(temp -> {
            try {
                temp = new HashMap<>(temp);
                temp.put("todayAmountLong", temp.get("todayAmount"));
                MerchantSaleTopDTO dto = mapper.readValue(mapper.writeValueAsString(temp), MerchantSaleTopDTO.class);
                dto.setTodayAmount(MoneyUtils.handleMoney(dto.getTodayAmountLong()));
                return dto;
            } catch (JsonProcessingException e) {
                return null;
            }
        }).collect(Collectors.toList());
        List<MerchantSaleTopVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    // 龙虎榜，列表数据
    @Override
    public List<RptDragonTigerCurrentVO> getDragonTigerList(String staffId, String merchantId) {
        List<RptDragonTigerCurrentVO> result = new ArrayList<>();
        DragonTigerStatisticsTypeEnum[] statisticsTypes = {COLLABORATE, SALEROOM, PROFIT, CROSS};
        LocalDate yesterday = LocalDate.now().minusDays(1);
        RptDragonTigerDay my = rptDragonTigerDayJpa.findAllByStaffIdAndDate(staffId, yesterday);
        for (DragonTigerStatisticsTypeEnum statisticsType : statisticsTypes) {
            RptDragonTigerCurrentVO vo = new RptDragonTigerCurrentVO();
            vo.setType(statisticsType.getCode());
            // 月最佳
            getMonthBest(statisticsType, getPreviousMonth(statisticsType), merchantId).ifPresent(temp -> {

                vo.setTopName(temp.getUserName());
                if (temp.getStaffId().equals(staffId)) {
                    vo.setEditType(1);
                } else {
                    vo.setEditType(0);
                }
                vo.setTopCount((Long) getValue(temp, statisticsType.getEntityAttributeName()));
                RptDragonTigerContent content = rptDragonTigerContentJpa.findByStaffIdAndMonthAndType(temp.getStaffId(), temp.getMonth(), statisticsType.getCode());
                if (content != null) {
                    vo.setRecollections(content.getContent());
                    vo.setId(content.getId());
                }
                vo.setMonth(temp.getMonth());

            });
            // 昨日最佳
            getDayBest(statisticsType, yesterday, merchantId).ifPresent(temp -> {
                vo.setYesterdayName(temp.getUserName());
                vo.setYesterdayCount((Long) getValue(temp, statisticsType.getEntityAttributeName()));
            });
            // 我的
            if (my != null) {
                vo.setMyName(my.getUserName());
                vo.setMyCount((Long) getValue(my, statisticsType.getEntityAttributeName()));
            }
            result.add(vo);
        }
        return result;
    }

    private String getPreviousMonth(DragonTigerStatisticsTypeEnum statisticsType) {
        switch (statisticsType) {
            case COLLABORATE:
                return DragonTigerStatisticsHelper.getPreviousMonthForCollaborate();
            case SALEROOM:
                return DragonTigerStatisticsHelper.getPreviousMonthForSale();
            case PROFIT:
                return DragonTigerStatisticsHelper.getPreviousMonthForProfit();
            case CROSS:
                return DragonTigerStatisticsHelper.getPreviousMonthForCross();
        }
        return null;
    }

    private Optional<RptDragonTigerDay> getDayBest(DragonTigerStatisticsTypeEnum tigerStatisticsType, LocalDate day, String merchantId) {
        Specification<RptDragonTigerDay> spec = (root, query, builder) -> builder.and(
                builder.equal(root.get("date"), day),
                builder.equal(root.get("merchantId"), merchantId)
        );
        return rptDragonTigerDayJpa
                .findAll(spec,
                        PageRequest.of(0, 1, Sort.Direction.DESC, tigerStatisticsType.getEntityAttributeName()))
                .get()
                .findAny();
    }

    private Optional<RptDragonTigerMonth> getMonthBest(DragonTigerStatisticsTypeEnum tigerStatisticsType, String month, String merchantId) {
        Specification<RptDragonTigerMonth> spec = (root, query, builder) -> builder.and(
                builder.equal(root.get("month"), month),
                builder.equal(root.get("merchantId"), merchantId)
        );
        return rptDragonTigerMonthJpa
                .findAll(spec,
                        PageRequest.of(0, 1, Sort.Direction.DESC, tigerStatisticsType.getEntityAttributeName()))
                .get()
                .findAny();
    }

    // 龙虎榜，历史数据
    @Override
    public List<RptDragonTigerHistoryVO> getHistoryList(String staffId, String merchantId, YearMonth hisMonth) {
        List<RptDragonTigerHistoryVO> result = new ArrayList<>();
        DragonTigerStatisticsTypeEnum[] statisticsTypes = {COLLABORATE, SALEROOM, PROFIT, CROSS};
        String his_Month = DateTimeFormatter.ofPattern("yyyy-MM").format(hisMonth);
        RptDragonTigerMonth my = rptDragonTigerMonthJpa.findAllByStaffIdAndMonth(staffId, his_Month);
        for (DragonTigerStatisticsTypeEnum statisticsType : statisticsTypes) {
            RptDragonTigerHistoryVO vo = new RptDragonTigerHistoryVO();
            vo.setType(statisticsType.getCode());
            // 历史月最佳
            getHistoryMonthBest(statisticsType, his_Month, merchantId).ifPresent(temp -> {
                vo.setTopName(temp.getUserName());
                vo.setTopCount((Long) getValue(temp, statisticsType.getEntityAttributeName()));
                RptDragonTigerContent content = rptDragonTigerContentJpa.findByStaffIdAndMonthAndType(temp.getStaffId(), temp.getMonth(), statisticsType.getCode());
                if (content != null) {
                    vo.setRecollections(content.getContent());
                }
            });
            // 我的
            if (my != null) {
                vo.setMyName(my.getUserName());
                vo.setMyCount((Long) getValue(my, statisticsType.getEntityAttributeName()));
            }
            result.add(vo);
        }
        return result;
    }

    private Optional<RptDragonTigerMonth> getHistoryMonthBest(DragonTigerStatisticsTypeEnum tigerStatisticsType, String his_Month, String merchantId) {
        Specification<RptDragonTigerMonth> spec = (root, query, builder) -> builder.and(
                builder.equal(root.get("month"), his_Month),
                builder.equal(root.get("merchantId"), merchantId)
        );
        return rptDragonTigerMonthJpa
                .findAll(spec,
                        PageRequest.of(0, 1, Sort.Direction.DESC, tigerStatisticsType.getEntityAttributeName()))
                .get()
                .findAny();
    }

    // app 统计报表
    @Override
    public Object getAppStaffAndShopResultInfo(AppStaffResultInfoForm form) {
//        String merchantId = "merchant111111";
//        String shopId = "shop111111";
//        String staffId = "mst506829670583508992";
        LocalDateTime[] time = AppStaffResultInfoHelper.getTime(form);
        LocalDateTime[] lastTime = AppStaffResultInfoHelper.getPreviousTime(form);
        List<Map<String, Object>> listShop;
        if (form.getRankType() == 0) {
            listShop = usrMerchantStaffJpa.getAppShopAllOrder(time[0], time[1], form.getGwMerchantId());
        } else {
            listShop = usrMerchantStaffJpa.getAppShopAllSale(time[0], time[1], form.getGwMerchantId());
        }
        listShop = listShop.stream().map(HashMap::new).collect(Collectors.toList());
        listShop.forEach(shop -> shop.put("saleNumLong", shop.get("saleNum")));
        AppShopResultInfoVO allShopData = new AppShopResultInfoVO();
        allShopData.setShops(jsonToList(listShop, new TypeReference<>() {
        }, new ArrayList<>()));
        if (form.getStatisticsType().equals(1)) {
            List<Map<String, Object>> listLast;
            if (form.getRankType() == 0) {
                listLast = usrMerchantStaffJpa.getAppShopAllOrder(lastTime[0], lastTime[1], form.getGwMerchantId());
            } else {
                listLast = usrMerchantStaffJpa.getAppShopAllSale(lastTime[0], lastTime[1], form.getGwMerchantId());
            }
            listLast = listLast.stream().map(HashMap::new).collect(Collectors.toList());
            listLast.forEach(shop -> shop.put("saleNumLong", shop.get("saleNum")));
            List<AppShopResultInfoVO.Shop> shops = jsonToList(listLast, new TypeReference<>() {
            }, new ArrayList<>());
            for (AppShopResultInfoVO.Shop sp : allShopData.getShops()) {
                for (AppShopResultInfoVO.Shop shop : shops) {
                    if (sp.getShopId().equals(shop.getShopId())) {
                        int spValue = sp.getShopRank() == null ? Integer.MAX_VALUE : sp.getShopRank();
                        int shopValue = shop.getShopRank() == null ? Integer.MAX_VALUE : shop.getShopRank();
                        sp.setRankValue(spValue > shopValue ? 0 : spValue == shopValue ? 2 : 1);
                    }
                }
            }

            allShopData.setOrderNum(allShopData.getShops().stream().mapToInt(AppShopResultInfoVO.Shop::getOrderNum).sum());
            allShopData.setSaleNumLong(allShopData.getShops().stream().mapToLong(AppShopResultInfoVO.Shop::getSaleNumLong).sum());

            int lastOrderNum = shops.stream().mapToInt(AppShopResultInfoVO.Shop::getOrderNum).sum();
            long lastSaleNum = shops.stream().mapToLong(AppShopResultInfoVO.Shop::getSaleNumLong).sum();
            allShopData.setOrderValue(allShopData.getOrderNum() > lastOrderNum ? 1 : allShopData.getOrderNum() == lastOrderNum ? 2 : 0);
            allShopData.setSaleValue(allShopData.getSaleNumLong() > lastSaleNum ? 1 : allShopData.getSaleNumLong() == lastSaleNum ? 2 : 0);
            allShopData.setSaleNum(MoneyUtils.handleMoney(allShopData.getSaleNumLong()));
            if (allShopData.getShops() != null) {
                allShopData.getShops().forEach(shop -> shop.setSaleNum(MoneyUtils.handleMoney(shop.getSaleNumLong())));
            }
            return allShopData;
        }
        List<Map<String, Object>> listStaff;
        if (form.getRankType() == 0) {
            listStaff = usrMerchantStaffJpa.getAppStaffAllOrderTop(time[0], time[1], form.getGwMerchantId());
        } else {
            listStaff = usrMerchantStaffJpa.getAppStaffAllSaleTop(time[0], time[1], form.getGwMerchantId());
        }
        listStaff = listStaff.stream().map(HashMap::new).collect(Collectors.toList());
        listStaff.forEach(shop -> shop.put("saleNumLong", shop.get("saleNum")));
        AppStaffResultInfoVO allStaffDate = new AppStaffResultInfoVO();
        List<AppStaffResultInfoVO.Staff> staffs = jsonToList(listStaff, new TypeReference<>() {
        }, new ArrayList<>());
        if (form.getOrgLvl().equals(0)) {
            if (StringUtils.isBlank(form.getGwShopId())) {
                throw new WarnException(ResultEnum.LOGIN_ERROR_ACCOUNT_NOT_EXIST);
            }
            staffs = staffs.stream()
                    .filter(temp -> form.getGwShopId().equals(temp.getShopId()))
                    .collect(Collectors.toList());
            AppStaffResultInfoVO.Staff staff = staffs.stream().filter(temp -> form.getGwMerchantStaffId().equals(temp.getStaffId())).findAny().orElse(null);
            if (staff == null) {
                throw new WarnException(ResultEnum.LOGIN_ERROR_ACCOUNT_NOT_EXIST);
            }
            int index = staffs.indexOf(staff);
            int start = Math.max(0, index - 3);
            int end = Math.min(index + 4, staffs.size());
            staffs = staffs.subList(start, end);
        } else if (form.getOrgLvl().equals(1)) {
            if (StringUtils.isNoneBlank(form.getShopId())) {
                staffs = staffs.stream()
                        .filter(temp -> form.getShopId().equals(temp.getShopId()))
                        .collect(Collectors.toList());
            }
        }
        allStaffDate.setStaff(staffs);
        String spId = form.getOrgLvl() == 0 ? form.getGwShopId() : form.getShopId();
        if (StringUtils.isNotBlank(spId)) {
            AppShopResultInfoVO.Shop shop = allShopData.getShops().stream()
                    .filter(temp -> spId.equals(temp.getShopId()))
                    .findAny().orElse(null);
            if (shop == null) {
                throw new WarnException(ResultEnum.LOGIN_ERROR_ACCOUNT_NOT_EXIST);
            }
            allStaffDate.setOrderNum(shop.getOrderNum());
            allStaffDate.setShopName(shop.getShopName());
            allStaffDate.setSaleNumLong(shop.getSaleNumLong());
            allStaffDate.setShopRank(shop.getShopRank());
        }
        allStaffDate.setSaleNum(MoneyUtils.handleMoney(allStaffDate.getSaleNumLong()));
        if (allStaffDate.getStaff() != null) {
            allStaffDate.getStaff().forEach(staff -> staff.setSaleNum(MoneyUtils.handleMoney(staff.getSaleNumLong())));
        }
        return allStaffDate;

    }

    // 月度平台总览
    @Override
    public MonthPlatListDTO getMonthPlatList(LocalDate date) {
        Map<String, Object> resp = new HashMap<>(rptPlatformDailySummaryJpa.getMonthPlatList(date));
        Integer mer = rptPlatformDailySummaryJpa.getMonthMctMerchant(date);
        ObjectMapper mapper = new ObjectMapper();
        try {
            resp.put("month_total_moneyLong", resp.get("month_total_money"));
            resp.put("plat_month_incomeLong", resp.get("plat_month_income"));
            resp.put("month_payLong", resp.get("month_pay"));
            resp.put("avg_month_moneyLong", resp.get("avg_month_money"));
            MonthPlatListDTO result = mapper.readValue(mapper.writeValueAsString(resp), MonthPlatListDTO.class);
            result.setMonth_total_money(MoneyUtils.handleMoney(result.getMonth_total_moneyLong()));
            result.setMonth_pay(MoneyUtils.handleMoney(result.getMonth_payLong()));
            if (mer == null || mer == 0) {
                return result;
            }
            BigDecimal wxIncome = new BigDecimal(result.getWei_xin());
            BigDecimal aliIncome = new BigDecimal(result.getAli());
            BigDecimal serIncome = new BigDecimal(result.getService_income());
            BigDecimal platIncome = wxIncome.add(aliIncome.add(serIncome));
            result.setPlat_month_income(MoneyUtils.handleMoney(platIncome.longValue()));
            result.setTotal_merchant(mer);
            BigDecimal total = new BigDecimal(mer);
            BigDecimal money = new BigDecimal(result.getMonth_total_money());
            BigDecimal avg = money.divide(total, 2, RoundingMode.HALF_UP);
            result.setAvg_month_money(MoneyUtils.handleMoney(avg.longValue()));
            return result;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    // 平台月度走势图/每日详情表
    @Override
    public List<MonthPlatTrendVO> getMonthPlatTrend(LocalDate date) {
        ObjectMapper mapper = new ObjectMapper();
        List<MonthPlatTrendDTO> all = rptPlatformDailySummaryJpa.getMonthPlatTrend(date).stream().map(temp -> {
            try {
                temp = new HashMap<>(temp);
                temp.put("total_moneyLong", temp.get("total_money"));
                MonthPlatTrendDTO stat = mapper.readValue(mapper.writeValueAsString(temp), MonthPlatTrendDTO.class);
                stat.setTotal_merchant(stat.getTotal_merchant());
                stat.setTotal_money(MoneyUtils.handleMoney(stat.getTotal_moneyLong()));
                stat.setDay(stat.getDay());
                return stat;
            } catch (JsonProcessingException e) {
                return null;
            }
        }).collect(Collectors.toList());
        List<MonthPlatTrendVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    // 平台月度商户成交额排行
    @Override
    public List<MonthMerchantSaleTopVO> getMonthMerchantSaleTop(YearMonth date) {
        ObjectMapper mapper = new ObjectMapper();
        List<MonthMerchantSaleTopVO> all = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
        try {
            List<Map<String, Object>> data = mctMerchantJpa.getMonthMerchantSaleTop(date.format(formatter)).stream().map(HashMap::new).collect(Collectors.toList());
            for (Map<String, Object> datum : data) {

                datum.put("todayAmountLong", datum.get("todayAmount"));
            }
            all = mapper.readValue(mapper.writeValueAsString(data),new TypeReference<>() {});
        } catch (JsonProcessingException e) {
            return new ArrayList<>();
        }
        if (all == null || all.isEmpty()) {
            return new ArrayList<>();
        }else{
            all.forEach(temp -> temp.setTodayAmount(MoneyUtils.handleMoney(temp.getTodayAmountLong())));
        }

        Set<String> merchantIds = all.stream().map(MonthMerchantSaleTopVO::getMerchant_id).collect(Collectors.toSet());
        YearMonth relativeMonth = date.minusYears(1);
        Map<String, Long> hisMonthMerchantSale = mctMerchantJpa.getHisMonthMerchantSale(relativeMonth.format(formatter), merchantIds)
                .stream()
                .collect(Collectors.toMap(temp -> (String) temp.get("merchant_id")
                        , temp -> Long.valueOf(temp.get("hisTodayAmount").toString())));
        all.forEach(item -> {
            BigDecimal cur = new BigDecimal(item.getTodayAmountLong());
            BigDecimal his = new BigDecimal(hisMonthMerchantSale.getOrDefault(item.getMerchant_id(), (long) 0));
            if (his.compareTo(BigDecimal.ZERO) == 0) {
                item.setVitality(null);
            } else {
                BigDecimal tong = cur.subtract(his).multiply(new BigDecimal(100)).divide(his, 2, RoundingMode.UP);
                item.setVitality(tong);
            }
        });
        return all;
    }

    //  店铺每日销售统计
    @Override
    public DayShopSaleTotalDTO getDayShopSaleTotal(LocalDate date, String shopId) {
        Map<String, Object> resp = rptshopSummaryJpa.getDayShopSaleTotal(date, shopId);
        ObjectMapper mapper = new ObjectMapper();
        try {
            DayShopSaleTotalDTO result = mapper.readValue(mapper.writeValueAsString(resp), DayShopSaleTotalDTO.class);
            return result;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }

    }

    //  店铺每日热销商品榜
    @Override
    public List<DayHotGoodsTopVO> getDayHotGoodsTop(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<DayHotGoodsTopDTO> all = rptGoodsSaleNumJpa.getDayHotGoodsTop(date, shopId).stream().map(temp -> {
            try {
                DayHotGoodsTopDTO stat = mapper.readValue(mapper.writeValueAsString(temp), DayHotGoodsTopDTO.class);
                return stat;
            } catch (JsonProcessingException e) {
                return null;
            }
        }).collect(Collectors.toList());
        List<DayHotGoodsTopVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    // 店铺员工每天销售情况统计
    @Override
    public List<DayStaffSaleVO> getDayStaffSale(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<DayStaffSaleDTO> all = rptStaffSaleInfoJpa.getDayStaffSale(date, shopId).stream().map(temp -> {
            try {
                DayStaffSaleDTO stat = mapper.readValue(mapper.writeValueAsString(temp), DayStaffSaleDTO.class);
                return stat;
            } catch (JsonProcessingException e) {
                return null;
            }
        }).collect(Collectors.toList());
        List<DayStaffSaleVO> voList = all.stream().map(this::po2vo).collect(Collectors.toList());
        return voList;
    }

    // 店铺销售/时间热力图
    @Override
    public List<SaleTimeHotMapVO> getSaleTimeHotMap(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptSaleHeatMapJpa.getSaleTimeHotMap(date, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺每日产品销售额占比
    @Override
    public List<DayShopSaleProVO> getDayShopSalePro(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptGoodsSaleNumJpa.getDayShopSalePro(date, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺每日商品日销高库存
    @Override
    public List<DayStockGoodsHighVO> getStockGoodsHigh(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptHighStockWarningJpa.getStockGoodsHigh(date, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺每周销售统计
    @Override
    public WeekShopSaleTotalVO getWeekShopSaleTotal(LocalDate date, String shopId) {
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        ObjectMapper mapper = new ObjectMapper();
        String mer_name = rptStaffSaleInfoJpa.getWeekStaffName(startDate, endDate, shopId);
        Map<String, Object> mer_else = rptshopSummaryJpa.getWeekShopSaleTotal(startDate, endDate, shopId);
        try {
            WeekShopSaleTotalVO elseVo = mapper.readValue(mapper.writeValueAsString(mer_else), WeekShopSaleTotalVO.class);
            elseVo.setName(mer_name);
            BigDecimal now_money = new BigDecimal(elseVo.getSale_money());
            LocalDate startDate_l = startDate.minusDays(7);
            LocalDate endDate_l = endDate.minusDays(7);
            Map<String, Object> mer_else2 = rptshopSummaryJpa.getWeekShopSaleTotal(startDate_l, endDate_l, shopId);
            WeekShopSaleTotalVO mer_else2Vo = mapper.readValue(mapper.writeValueAsString(mer_else2), new TypeReference<>() {
            });

            BigDecimal last_money = new BigDecimal(mer_else2Vo.getSale_money());
            BigDecimal add_money = now_money.subtract(last_money);
            elseVo.setAdd_money(add_money.longValue());
            return elseVo;
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return null;
        }
    }

    // 店铺每周热销商品榜
    @Override
    public List<DayHotGoodsTopVO> getWeekHotGoodsTop(LocalDate date, String shopId) {
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptGoodsSaleNumJpa.getWeekHotGoodsTop(startDate, endDate, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 每周店铺销售走势图
    @Override
    public List<WeekShopSaleCurveVO> getWeekShopSaleCurve(LocalDate date, String shopId) {
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptshopSummaryJpa.getWeekShopSaleCurve(startDate, endDate, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺每周产品销售额占比
    @Override
    public List<DayShopSaleProVO> getWeekShopSalePro(LocalDate date, String shopId) {
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptGoodsSaleNumJpa.getWeekShopSalePro(startDate, endDate, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺每周店员销售统计
    @Override
    public List<WeekStaffNameNumVO> getWeekStaffNameNum(LocalDate date, String shopId) {
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptStaffSaleInfoJpa.getWeekStaffNameNum(startDate, endDate, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺每周商品日销高库存
    @Override
    public List<WeekStockGoodsHighVO> getWeekStockGoodsHigh(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        DayOfWeek week = date.getDayOfWeek();
        LocalDate endDate = date.plusDays(7 - week.getValue());
        List<Map<String, Object>> mapList = rptHighStockWarningJpa.getWeekStockGoodsHigh(endDate, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺每月销售统计
    @Override
    public MonthShopSaleTotalVO getMonthShopSaleTotal(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> mer = rptshopSummaryJpa.getMonthShopSaleTotal(date, shopId);
        if (mer.isEmpty()) {
            return null;
        }
        try {
            MonthShopSaleTotalVO resultVO = mapper.readValue(mapper.writeValueAsString(mer), new TypeReference<>() {
            });
            BigDecimal now_money = new BigDecimal(resultVO.getSaleMoney());
            LocalDate relativeMonth = date.minusYears(1);
            Map<String, Object> mer2 = rptshopSummaryJpa.getMonthShopSaleTotal(relativeMonth, shopId);
            MonthShopSaleTotalVO resultVO2 = mapper.readValue(mapper.writeValueAsString(mer2), new TypeReference<>() {
            });
            BigDecimal last_money = new BigDecimal(resultVO2.getSaleMoney() == null ? 0 : resultVO2.getSaleMoney());
            BigDecimal add_money = now_money.subtract(last_money);
            resultVO.setAddMoney(add_money.longValue());

            return resultVO;
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return null;
        }
    }

    // 店铺月度每日营业额走势图
    @Override
    public List<WeekShopSaleCurveVO> getMonthShopSaleCurve(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptshopSummaryJpa.getMonthShopSaleCurve(date, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺月度每日销售详情
    @Override
    public List<MonthShopSaleDetailVO> getMonthShopSaleDetail(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptshopSummaryJpa.getMonthShopSaleDetail(date, shopId);
        List<Map<String, Object>> mapList1 = rptGoodsSaleNumJpa.getMonthHotGoodsTop(date, shopId);
        try {
            List<MonthShopSaleDetailVO> resultVo = mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
            List<MonthShopSaleDetailVO> resultVo2 = mapper.readValue(mapper.writeValueAsString(mapList1), new TypeReference<>() {
            });
            resultVo.forEach(temp -> resultVo2.forEach(temp2 -> {
                if (temp.getDay().equals(temp2.getDay())) {
                    temp.setTotalHotName(temp2.getTotalHotName());
                }
            }));
            return resultVo;
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 每月员工业绩排行榜
    public List<MonthStaffSaleTopVO> getMonthStaffSaleTop(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptStaffSaleInfoJpa.getMonthStaffSaleTop(date, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺年度销售统计
    @Override
    public YearShopSaleTotalVO getYearShopSaleTotal(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> mer = rptshopSummaryJpa.getYearShopSaleTotal(date, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mer), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return null;
        }
    }

    // 店铺年度每月营业额走势图
    @Override
    public List<YearShopSaleCurveVO> getYearShopSaleCurve(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptshopSummaryJpa.getYearShopSaleCurve(date, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺年度每月销售详情
    @Override
    public List<YearShopSaleDetailVO> getYearShopSaleDetail(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptshopSummaryJpa.getYearShopSaleDetail(date, shopId);
        List<Map<String, Object>> mapList1 = rptGoodsSaleNumJpa.getYearHotGoodsTop(date, shopId);
        try {
            List<YearShopSaleDetailVO> resultVo = mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
            List<YearShopSaleDetailVO> resultVo2 = mapper.readValue(mapper.writeValueAsString(mapList1), new TypeReference<>() {
            });
            resultVo.forEach(temp -> resultVo2.forEach(temp2 -> {
                if (temp.getMon().equals(temp2.getMon())) {
                    temp.setTotalHotName(temp2.getTotalHotName());
                }
            }));
            return resultVo;
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 年度员工业绩排行榜
    public List<MonthStaffSaleTopVO> getYearStaffSaleTop(LocalDate date, String shopId) {
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptStaffSaleInfoJpa.getYearStaffSaleTop(date, shopId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 员工每周销售量TOP
    @Override
    public List<StaffHotGoodsTopNumVO> getStaffHotGoodsNumTop(LocalDate date, String staffId) {
        ObjectMapper mapper = new ObjectMapper();
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        List<Map<String, Object>> mapList = rptStaffHotSaleJpa.getStaffHotGoodsNumTop(startDate, endDate, staffId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 员工每周销售额TOP
    @Override
    public List<StaffHotGoodsMoneyTopVO> getStaffHotGoodsMoneyTop(LocalDate date, String staffId) {
        ObjectMapper mapper = new ObjectMapper();
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        List<Map<String, Object>> mapList = rptStaffHotSaleJpa.getStaffHotGoodsMoneyTop(startDate, endDate, staffId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 店铺每周单个店员销售统计（用于员工报表）
    @Override
    public StaffWeekSaleVO getStaffWeekSale(LocalDate date, String staffId) {
        ObjectMapper mapper = new ObjectMapper();
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        Map<String, Object> mer = rptStaffSaleInfoJpa.getStaffWeekSale(startDate, endDate, staffId);
        try {
            return mapper.readValue(mapper.writeValueAsString(mer), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return null;
        }
    }

    // 店铺每周热销商品榜(员工角度)
    @Override
    public List<DayHotGoodsTopVO> getStaffWeekHotGoodsTop(LocalDate date, String staffId) {
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        ObjectMapper mapper = new ObjectMapper();
        UsrMerchantStaff shopStaff = usrMerchantStaffJpa.findById(staffId).orElse(null);
        if (shopStaff == null) {
            return new ArrayList<>();
        }
        List<Map<String, Object>> mapList = rptGoodsSaleNumJpa.getWeekHotGoodsTop(startDate, endDate, shopStaff.getShopId());
        try {
            return mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return new ArrayList<>();
        }
    }

    // 单一员工每日销售概览
    @Override
    public List<StaffDaySaleVO> getStaffDaySale(LocalDate date, String staffId) {
        DayOfWeek week = date.getDayOfWeek();
        LocalDate startDate = date.minusDays(week.getValue() - 1);
        LocalDate endDate = date.plusDays(7 - week.getValue());
        ObjectMapper mapper = new ObjectMapper();
        List<Map<String, Object>> mapList = rptStaffSaleInfoJpa.getStaffDaySale(startDate, endDate, staffId);
        try {
            List<StaffDaySaleVO> result = mapper.readValue(mapper.writeValueAsString(mapList), new TypeReference<>() {
            });
            LocalDate startDate_l = startDate.minusDays(7);
            LocalDate endDate_l = endDate.minusDays(7);
            List<Map<String, Object>> mapList1 = rptStaffSaleInfoJpa.getStaffDaySale(startDate_l, endDate_l, staffId);
            List<StaffDaySaleVO> result1 = mapper.readValue(mapper.writeValueAsString(mapList1), new TypeReference<>() {
            });
            result.forEach(temp -> result1.forEach(temp1 -> {
                if (temp1.getSaleMoney() == null) {
                    temp.setVitality(null);
                } else {
                    BigDecimal now_money = new BigDecimal(temp.getSaleMoney());
                    BigDecimal last_money = new BigDecimal(temp1.getSaleMoney());
                    BigDecimal tong = now_money.subtract(last_money).multiply(new BigDecimal(100)).
                            divide(last_money, 2, RoundingMode.UP);
                    temp.setVitality(tong);
                }
            }));
            return result;
        } catch (JsonProcessingException e) {
            log.error("json read error,sql c", e);
            return null;
        }
    }

    /**
     * @param po: 数据库实体类
     * @description: 数据库实体类转换为VO类
     * @return: com.bcb.user.common.vo.RptPlatformCategoryInfoVO
     */
    private RptPlatformCategoryInfoVO po2vo(RptPlatformCategoryPercent po) {
        RptPlatformCategoryInfoVO vo = new RptPlatformCategoryInfoVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private RptGoodsSaleNumVO po2vo(RptGoodsSaleNum po) {
        RptGoodsSaleNumVO vo = new RptGoodsSaleNumVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }


    private RptLowStockWarningVO po2vo(RptLowStockWarning po) {
        RptLowStockWarningVO vo = new RptLowStockWarningVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private RptStaffHotSaleVO po2vo(RptStaffHotSale po) {
        RptStaffHotSaleVO vo = new RptStaffHotSaleVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private RptStaffSaleInfoVO po2vo(RptStaffSaleInfo po) {
        RptStaffSaleInfoVO vo = new RptStaffSaleInfoVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private MonthPlatSaleVO po2vo(MonthPlatSaleDTO po) {
        MonthPlatSaleVO vo = new MonthPlatSaleVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private PlatIncomePayVO po2vo(PlatIncomePayDTO po) {
        PlatIncomePayVO vo = new PlatIncomePayVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private MerchantSaleTopVO po2vo(MerchantSaleTopDTO po) {
        MerchantSaleTopVO vo = new MerchantSaleTopVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private MonthPlatTrendVO po2vo(MonthPlatTrendDTO po) {
        MonthPlatTrendVO vo = new MonthPlatTrendVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private DayHotGoodsTopVO po2vo(DayHotGoodsTopDTO po) {
        DayHotGoodsTopVO vo = new DayHotGoodsTopVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private DayStaffSaleVO po2vo(DayStaffSaleDTO po) {
        DayStaffSaleVO vo = new DayStaffSaleVO();
        BeanUtils.copyProperties(po, vo);
        return vo;
    }

    private static <T> List<T> jsonToList(List<Map<String, Object>> list, TypeReference<List<T>> tf, List<T> defaultValue) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(mapper.writeValueAsString(list), tf);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }
}
