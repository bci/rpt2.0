package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptDragonTigerDay;
import com.bcb.rpt.server.entity.RptDragonTigerMonth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.time.LocalDate;
import java.util.List;

public interface RptDragonTigerDayJpa extends JpaRepository<RptDragonTigerDay, String>,
        JpaSpecificationExecutor<RptDragonTigerDay> {
    RptDragonTigerDay findAllByStaffIdAndDate(String month, LocalDate date);


}
