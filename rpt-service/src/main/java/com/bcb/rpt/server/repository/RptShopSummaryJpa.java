package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptShopSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface RptShopSummaryJpa extends JpaRepository <RptShopSummary, String>{
    Optional<RptShopSummary> findByDateAndShopId(LocalDate date, String shopId);

    // 店铺每日销售统计
    @Query(nativeQuery = true, value = "SELECT " +
    "sale_num sale_num," +
    "sale_money sale_money," +
    "bill_num bill_num," +
    "refund_num refund_num," +
    "refund_money refund_money," +
    "pre_num pre_num," +
    "pre_money pre_money " +
    "FROM rpt_shop_summary " +
    "where date = :date and shop_id = :shopId")
    Map<String, Object> getDayShopSaleTotal(@Param("date")LocalDate date,@Param("shopId")String shopId);

    // 店铺每周销售统计
    @Query(nativeQuery = true, value = "select " +
    "ifnull(SUM(sale_num),0) sale_num," +
    "ifnull(sum(sale_money),0) sale_money," +
    "ifnull(sum(purchase_expenses),0) purchase_expenses " +
    "from rpt_shop_summary " +
    "where date between :startDate and :endDate and shop_id = :shopId")
    Map<String, Object> getWeekShopSaleTotal(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                             @Param("shopId")String shopId);

    // 每周店铺销售走势图
    @Query(nativeQuery = true, value = "select " +
    "sale_num saleNum," +
    "sale_money saleMoney, " +
    "date_format(date, '%m-%d') day" +
    " from rpt_shop_summary " +
    "where date between :startDate and :endDate and shop_id = :shopId" +
    " group by day, sale_money, sale_num " +
    " order by day asc ")
    List<Map<String, Object>> getWeekShopSaleCurve(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                                   @Param("shopId")String shopId);

    // 店铺每月销售统计
    @Query(nativeQuery = true, value = "select " +
    "ifnull(sum(sale_num),0) saleNum," +
    "ifnull(sum(sale_money),0) saleMoney," +
    "ifnull(sum(purchase_expenses),0) purchaseExpenses," +
    "ifnull(sum(bill_money),0) billMoney, " +
    "date_format(date, '%Y-%m') month " +
    "from rpt_shop_summary " +
    "where date_format(date, '%Y-%m') = date_format(:date, '%Y-%m') and shop_id = :shopId" +
    " group by month")
    Map<String, Object> getMonthShopSaleTotal(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 店铺月度每日营业额走势图
    @Query(nativeQuery = true, value = "select " +
    "sale_money saleMoney," +
    "sale_num saleNum," +
    "date_format(date, '%m-%d') day " +
    "from rpt_shop_summary " +
    " where date_format(date, '%Y-%m') = date_format(:date, '%Y-%m') and shop_id = :shopId" +
    " group by day, saleMoney, saleNum" +
    " order by day asc")
    List<Map<String, Object>> getMonthShopSaleCurve(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 店铺月度每日销售详情
    @Query(nativeQuery = true, value = "select " +
    "sale_money saleMoney," +
    "bill_num billNum," +
    "purchase_expenses purchaseExpenses, " +
            "date_format(date, '%m-%d') day " +
            "from rpt_shop_summary " +
    "where date_format(date, '%Y-%m') = date_format(:date, '%Y-%m') and shop_id = :shopId" +
    " order by day asc")
    List<Map<String, Object>> getMonthShopSaleDetail(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 店铺年度销售统计
    @Query(nativeQuery = true, value = "select " +
    "sum(sale_num) saleNum," +
    "sum(sale_money) saleMoney," +
    "sum(bill_money) billMoney," +
    "sum(purchase_expenses) purchaseExpenses " +
    "from rpt_shop_summary " +
    "where date_format(date, '%Y') = date_format(:date, '%Y') and shop_id = :shopId")
    Map<String, Object> getYearShopSaleTotal(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 店铺年度每月营业额走势图
    @Query(nativeQuery = true, value = "select " +
    "sum(sale_money) saleMoney," +
    "sum(sale_num) saleNum," +
    "date_format(date, '%Y-%m') month " +
    "from rpt_shop_summary " +
    "where date_format(date, '%Y') = date_format(:date, '%Y') and shop_id = :shopId" +
    " group by month " +
    "order by month asc ")
    List<Map<String, Object>> getYearShopSaleCurve(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 店铺年度每月销售详情
    @Query(nativeQuery = true, value = "select " +
            "sum(sale_money) saleMoney," +
            "sum(bill_num) billNum," +
            "sum(purchase_expenses) purchaseExpenses, " +
            "date_format(date, '%Y-%m') mon " +
            "from rpt_shop_summary " +
            "where date_format(date, '%Y') = date_format(:date, '%Y') and shop_id = :shopId" +
            " group by mon" +
            " order by mon asc")
    List<Map<String, Object>> getYearShopSaleDetail(@Param("date")LocalDate date, @Param("shopId")String shopId);

}

