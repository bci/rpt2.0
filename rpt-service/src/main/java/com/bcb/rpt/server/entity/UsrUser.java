package com.bcb.rpt.server.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: 用户表 ( usr_user )
 * @date 2020-07-07 16:09
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class UsrUser {

    /**
     * id
     */
    @Id
    private String id;

    /**
     * 用户账号
     */
    private String account;

    /**
     * 性别（0：未知；1：男；2：女）
     */
    private Integer gender;

    /**
     * 用户头像
     */
    private String headImg;

    /**
     * 用户手机号码
     */
    private String mobilePhone;

    /**
     * 用户姓名
     */
    private String name;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 用户级别
     */
    private Integer level;

    /**
     * 用户状态（0:禁用,1:可用）
     */
    private Integer status;

    /**
     * 删除标识（0：未删除；其他：已删除，时间戳）
     */
    private Integer isDelete;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;
}
