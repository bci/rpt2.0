package com.bcb.rpt.server.controller;

import com.bcb.commom.vo.ResultVO;
import com.bcb.rpt.common.form.RptDragonTigerContentForm;
import com.bcb.rpt.server.dto.RptDragonTigerContentDTO;
import com.bcb.rpt.server.service.RptInsertService;
import com.bcb.rpt.server.util.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@Slf4j
@Validated
@RequestMapping("/rpt")
public class RptInsertServiceController {
    @Resource
    RptInsertService rptAdminService;

    @PostMapping("/dragonTiger/getExitRecollections")
    public ResultVO<String> getExitRecollections(@Valid RptDragonTigerContentForm rptDragonTigerContentForm) {
        RptDragonTigerContentDTO dto = new RptDragonTigerContentDTO();
        BeanUtils.copyProperties(rptDragonTigerContentForm, dto);
        dto.setStaffId(rptDragonTigerContentForm.getGwMerchantStaffId());
        return ResultUtil.success(rptAdminService.insertExitRecollection(dto));
    }

    @PostMapping("/dragonTiger/deleteContent")
    public ResultVO<String> deleteContent(String id) {
        rptAdminService.delete(id);
        return ResultUtil.success("删除成功");
    }

}
