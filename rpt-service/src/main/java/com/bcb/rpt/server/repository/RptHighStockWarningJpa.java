package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptHighStockWarning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.time.LocalDate;
import java.util.Map;

public interface RptHighStockWarningJpa extends JpaRepository<RptHighStockWarning, String> {

    // 店铺每日商品日销高库存
    @Query(nativeQuery = true, value = "SELECT " +
            "goods_name goods_name," +
            "goods_id goods_id," +
            "on_store_num on_store_num," +
            "lately_sale_num lately_sale_num " +
            "FROM rpt_high_stock_warning " +
    "where date = :date and shop_id = :shopId" +
    " order by on_store_num desc")
    List<Map<String, Object>> getStockGoodsHigh(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 店铺每周商品日销高库存
    @Query(nativeQuery = true, value = "SELECT " +
            "goods_name goods_name," +
            "goods_id goods_id," +
            "on_store_num on_store_num " +
            "FROM rpt_high_stock_warning " +
            "where date = :endDate and shop_id = :shopId" +
            " order by on_store_num desc ")
    List<Map<String, Object>> getWeekStockGoodsHigh(@Param("endDate")LocalDate endDate, @Param("shopId")String shopId);

    // 店铺每日高库存预警
    @Query(nativeQuery = true, value = "select "+
    "goods_name goodsName," +
    "on_store_num onStoreNum " +
    "from rpt_high_stock_warning " +
    "where date = :date and shop_id = :shopId " +
    "order by on_store_num desc limit 7")
    List<Map<String, Object>> getHighStockWarning(@Param("date")LocalDate date, @Param("shopId")String shopId);

    @Query(nativeQuery = true, value = "select "+
            "sum(on_store_num) onStoreNum " +
            "from rpt_high_stock_warning " +
            "where date = :date and shop_id = :shopId ")
    Integer getStockGoodsAllNum(@Param("date")LocalDate date, @Param("shopId")String shopId);
}
