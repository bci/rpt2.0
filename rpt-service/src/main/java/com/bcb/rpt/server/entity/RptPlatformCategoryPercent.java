package com.bcb.rpt.server.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import java.lang.Long;
import javax.persistence.Entity;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.EntityListeners;
import javax.persistence.Id;


/**
 * @author Mr.Ma
 * @description: 记录平台内销售商品所属分类，主要是汇总扇形占比图用 ( rpt_platform_category_percent )
 * @date 2020-05-16 14:59
 */
@Data // lombok注解 生成实体类方法
@Entity // jpa注解 表明是一个实体类 即一张表
@DynamicInsert // jpa注解 null值不插入
@EntityListeners(AuditingEntityListener.class) // 审计功能注解 自动填充时间等信息
public class RptPlatformCategoryPercent {
    
    /**
     * id
     */
    @Id
    private String id;
    

    /**
    * 关联品类id
    */
    private String categoryId;

    /**
     * 关联品类名字
     */
    private String categoryName;

    /**
    * 该品类商品的总销售额
    */
    private Long totalMoney;

    /**
    * 该品类商品的总销量
    */
    private Integer totalNum;

    /**
    * 数据统计时间(精确到天)
    */
    private LocalDateTime createTime;

    /**
    * 数据最后一次更新时间
    */
    private LocalDateTime updateTime;

    /**
    * 版本控制锁（乐观锁），默认值0
    */
    private Integer version;

    /**
    * 该条数据的归属时间
    */
    private LocalDate date;

}