package com.bcb.rpt.server.config;

import com.bcb.commom.filter.GatewayGeneratedHeadersHolder;
import com.bcb.commom.filter.GetHeader;
import lombok.extern.slf4j.Slf4j;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

/**
 * @author chiangtaol
 * @date 2020-04-29
 * @describe 将Gateway产生的Header参数设置到form中
 */
@Slf4j
public class GatewayGeneratedHeadersWrapper extends HttpServletRequestWrapper {
    private GatewayGeneratedHeadersHolder<HttpServletRequest> holder = new GatewayGeneratedHeadersHolder<HttpServletRequest>();

    private static final GetHeader<HttpServletRequest> getter = new GetHeaderForHttpServletRequest();
    private static class GetHeaderForHttpServletRequest implements GetHeader<HttpServletRequest> {
        @Override
        public String getHeader(HttpServletRequest t, String name){
            return t.getHeader(name);
        }
    }

    public GatewayGeneratedHeadersWrapper(HttpServletRequest request) {
        super(request);

        holder.init(getter, request);
    }

    @Override
    public Enumeration<String> getParameterNames() {
        Enumeration<String> enumeration = super.getParameterNames();
        ArrayList<String> list = Collections.list(enumeration);

        list.addAll(holder.getParameterNames());
        return Collections.enumeration(list);
    }

    @Override
    public String[] getParameterValues(String name) {
        // 先从holder中获取
        String[] r = holder.getParameterValues(name);

        if(null != r){
            return r;
        } else {
            return super.getParameterValues(name);
        }
    }

}
