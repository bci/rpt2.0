package com.bcb.rpt.server.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import java.lang.Long;
import javax.persistence.Entity;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.EntityListeners;
import javax.persistence.Id;


/**
 * @author Mr.Ma
 * @description: 店铺每日汇总表 ( rpt_shop_summary )
 * @date 2020-05-16 11:15
 */
@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class RptShopSummary {
    
    /**
     * id
     */
    @Id
    private String id;
    

    /**
    * 关联店铺表id
    */
    private String shopId;

    /**
    * 关联商户表id
    */
    private String merchantId;

    /**
    * 店铺当日总销量
    */
    private Integer saleNum;

    /**
    * 店铺当天总销售额
    */
    private Long saleMoney;

    /**
    * 当日开具发票总额
    */
    private Long billMoney;

    /**
    * 当日开具发票量
    */
    private Integer billNum;

    /**
    * 当日进/购货总支出
    */
    private Long purchaseExpenses;

    /**
    * 当天退货单数
    */
    private Integer refundNum;

    /**
    * 当日退货总额
    */
    private Long refundMoney;

    /**
    * 当日换货总单数
    */
    private Integer exchangeNum;

    /**
    * 当日待换货总额
    */
    private Long exchangeStayMoney;

    /**
     * 当日已换货总额
     */
    private Long exchangeEndMoney;

    /**
    * 当天预售数量
    */
    private Integer preNum;

    /**
    * 当日预售商品总额
    */
    private Long preMoney;

    /**
    * 数据统计日期(精确到天即可)
    */
    private LocalDateTime createTime;

    /**
    * 数据更新时间
    */
    private LocalDateTime updateTime;

    /**
    * 版本控制锁（乐观锁）,默认值0
    */
    private Integer version;

    /**
    * 该条数据的归属时间
    */
    private LocalDate date;

}