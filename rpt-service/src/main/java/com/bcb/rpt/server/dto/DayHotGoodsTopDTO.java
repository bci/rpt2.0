package com.bcb.rpt.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DayHotGoodsTopDTO {
    private String goods_id;
    private String name;
    private Integer sale_num;
    private Long sale_money;

}
