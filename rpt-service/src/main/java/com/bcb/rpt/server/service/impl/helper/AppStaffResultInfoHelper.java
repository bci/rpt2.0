package com.bcb.rpt.server.service.impl.helper;

import com.bcb.rpt.common.form.AppStaffResultInfoForm;
import com.bcb.rpt.server.exception.WarnException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.YearMonth;

public class AppStaffResultInfoHelper {
    public static LocalDateTime[] getTime(AppStaffResultInfoForm form) throws WarnException {
        LocalDateTime[] result = new LocalDateTime[2];
        switch (form.getPeriod()) {
            case 0:
                LocalDate nowDay = LocalDate.now();
                result[0] = nowDay.atTime(0, 0, 0);
                result[1] = nowDay.atTime(23, 59, 59);
                break;
            case 1:
                YearMonth nowMonth = YearMonth.now();
                result[0] = nowMonth.atDay(1).atTime(0, 0, 0);
                result[1] = nowMonth.atEndOfMonth().atTime(23, 59, 59);
                break;
            case 2:
                Year nowYear = Year.now();
                result[0] = nowYear.atDay(1).atTime(0, 0, 0);
                result[1] = nowYear.atMonth(12).atEndOfMonth().atTime(23, 59, 59);
                break;
            default:
                throw new WarnException("999999", "统计周期枚举值错误");
        }
        return result;
    }
    public static LocalDateTime[] getPreviousTime(AppStaffResultInfoForm form) throws WarnException {
        LocalDateTime[] result = new LocalDateTime[2];
        switch (form.getPeriod()) {
            case 0:
                LocalDate nowDay = LocalDate.now().minusDays(1);
                result[0] = nowDay.atTime(0, 0, 0);
                result[1] = nowDay.atTime(23, 59, 59);
                break;
            case 1:
                YearMonth nowMonth = YearMonth.now().minusMonths(1);
                result[0] = nowMonth.atDay(1).atTime(0, 0, 0);
                result[1] = nowMonth.atEndOfMonth().atTime(23, 59, 59);
                break;
            case 2:
                Year nowYear = Year.now().minusYears(1);
                result[0] = nowYear.atDay(1).atTime(0, 0, 0);
                result[1] = nowYear.atMonth(12).atEndOfMonth().atTime(23, 59, 59);
                break;
            default:
                throw new WarnException("999999", "统计周期枚举值错误");
        }
        return result;
    }
}
