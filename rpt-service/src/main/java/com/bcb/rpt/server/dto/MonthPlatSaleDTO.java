package com.bcb.rpt.server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * @author Mr.Li
 * @description: 平台月度成交额统计
 * @date 2020-07-07 16:09
 */

@Getter
@Setter
@ToString
public class MonthPlatSaleDTO {
    private String month_total_money;
    private Long month_total_moneyLong;
    private String month;
}
