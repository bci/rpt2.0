package com.bcb.rpt.server.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: app龙虎榜日表 ( rpt_dragon_tiger_day )
 * @date 2020-07-27 09:59
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class RptDragonTigerDay {
    /**
     * id
     */
    @Id
    private String id;

    /**
     * 员工id
     */
    private String staffId;

    /**
     * 商户id
     */
    private String merchantId;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 协作次数
     */
    private Long collaboration;

    /**
     * 销售金额
     */
    private Long saleMoney;

    /**
     * 利润金额
     */
    private Long profitMoney;

    /**
     * 满意度
     */
    private Long satisfaction;

    /**
     * 跨部门销售金额
     */
    private Long crossDepartmentSales;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
