package com.bcb.rpt.server.util;

import com.bcb.commom.util.IdUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author: Mr.Yuan
 * @date: 2020-05-09
 * @description:
 */
@Component
public class RtpIdUtil {

    private static final IdUtil idUtil = new IdUtil(2,3);

    /**
     * @description: 生成表ID
     * @params:
     *   prefix: ID前缀，需使用UsrConst类中USER_TABLE_ID_PRE_*
     * @return: java.lang.String
     */
    public String createId(String prefix){
        return prefix + idUtil.nextId();
    }
}
