package com.bcb.rpt.server.service;

import com.bcb.rpt.common.form.AppStaffResultInfoForm;
import com.bcb.rpt.common.vo.*;
import com.bcb.rpt.server.dto.DayShopSaleTotalDTO;
import com.bcb.rpt.server.dto.MonthPlatListDTO;
import com.bcb.rpt.server.dto.PlatFormMctChantStatDTO;
import com.bcb.rpt.server.dto.PlatFormSaleStatDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.List;

public interface RptAdminService {
    /**
     * @param date:
     * @description: 定义数据返回路径连接
     * @return:
     */
    //  平台每日交易信息统计
    RptPlatformDailySummaryVO getPlatformDailySummary(LocalDate date);

    //  记录平台内销售商品所属分类，主要是汇总扇形占比图
    List<RptPlatformCategoryInfoVO> getPlatformCategoryInfo(LocalDate data);

    //  店铺每日汇总
    RptShopSummaryVO getShopSummary(LocalDate date, String shopId);

    //  店铺每日各个商品销售量
    List<RptGoodsSaleNumVO> getGoodsSaleNum(LocalDate date, String shopId);

    //  高库存商品预警
    RptHighStockWarningVO getHighStockWarning(LocalDate date, String shopId);

    //  周 高库存商品预警
    RptHighStockWarningVO getWeekHighStockWarning(LocalDate date, String shopId);

    //  低库存商品预警
    List<RptLowStockWarningVO> getLowStockWarning(LocalDate date, String shopId);

    //  店铺销售热力图
    RptSaleHeatMapVO getSaleHeatMap(LocalDateTime date, String shopId);

    //  员工热卖商品榜统计
    List<RptStaffHotSaleVO> getStaffHotSale(LocalDate date, String staffId);

    //  店铺每日员工销售情况统计
    List<RptStaffSaleInfoVO> getStaffSaleInfo(LocalDate date, String shopId);

    //  平台每日实时成交额
    long getRealTimeAmount(LocalDate updateTime);

    //  平台每日实时入驻商户数量
    long getRealTimeMctMerchant(LocalDate updateTime);

    //  平台销售情况汇总
    PlatFormSaleStatDTO getPlatFormSaleStat(LocalDate updateTime);

    //  平台商户情况统计
    PlatFormMctChantStatDTO getPlatFormMerchantStat(LocalDate updateTime);

    //  平台月度成交额统计
    List<MonthPlatSaleVO> getMonthPlatSale(LocalDate date);

    //   平台收入支出情况统计
    List<PlatIncomePayVO> getPlatIncomePay(LocalDate date);

    //  平台商户当天成交额排行
    List<MerchantSaleTopVO> getMerchantSaleTop(LocalDate updateTime);

    //  龙虎榜列表
    List<RptDragonTigerCurrentVO> getDragonTigerList(String staffId, String merchantId);

    // 龙虎榜历史榜单
    List<RptDragonTigerHistoryVO> getHistoryList(String staffId, String merchantId, YearMonth hisMonth);

    // app 统计报表
    Object getAppStaffAndShopResultInfo(AppStaffResultInfoForm form);

    // 平台月度总览
    MonthPlatListDTO getMonthPlatList(LocalDate date);

    // 平台月度走势图/每日详情表
    List<MonthPlatTrendVO> getMonthPlatTrend(LocalDate date);

    // 平台月度商户成交额排行
    List<MonthMerchantSaleTopVO> getMonthMerchantSaleTop(YearMonth date);

    // 店铺每日销售统计
    DayShopSaleTotalDTO getDayShopSaleTotal(LocalDate date, String shopId);

    // 店铺每日热销商品榜
    List<DayHotGoodsTopVO> getDayHotGoodsTop(LocalDate date, String shopId);

    // 店铺员工每天销售情况统计
    List<DayStaffSaleVO> getDayStaffSale(LocalDate date, String shopId);

    // 店铺销售/时间热力图
    List<SaleTimeHotMapVO> getSaleTimeHotMap(LocalDate date, String shopId);

    // 店铺每日产品销售额占比
    List<DayShopSaleProVO> getDayShopSalePro(LocalDate date, String shopId);

    // 店铺每日商品日销高库存
    List<DayStockGoodsHighVO> getStockGoodsHigh(LocalDate date, String shopId);

    // 店铺每周销售统计
    WeekShopSaleTotalVO getWeekShopSaleTotal(LocalDate date, String shopId);

    // 店铺每周热销商品榜
    List<DayHotGoodsTopVO> getWeekHotGoodsTop(LocalDate date, String shopId);

    // 每周店铺销售走势图
    List<WeekShopSaleCurveVO> getWeekShopSaleCurve(LocalDate date, String shopId);

    // 店铺每周产品销售额占比
    List<DayShopSaleProVO> getWeekShopSalePro(LocalDate date, String shopId);

    // 店铺每周店员销售统计
    List<WeekStaffNameNumVO> getWeekStaffNameNum(LocalDate date, String shopId);

    // 店铺每周商品日销高库存
    List<WeekStockGoodsHighVO> getWeekStockGoodsHigh(LocalDate date, String shopId);

    // 店铺每月销售统计
    MonthShopSaleTotalVO getMonthShopSaleTotal(LocalDate date, String shopId);

    // 店铺月度每日营业额走势图
    List<WeekShopSaleCurveVO> getMonthShopSaleCurve(LocalDate date, String shopId);

    // 店铺月度每日销售详情
    List<MonthShopSaleDetailVO> getMonthShopSaleDetail(LocalDate date, String shopId);

    // 每月员工业绩排行榜
    List<MonthStaffSaleTopVO> getMonthStaffSaleTop(LocalDate date, String shopId);

    // 店铺年度销售统计
    YearShopSaleTotalVO getYearShopSaleTotal(LocalDate date, String shopId);

    // 店铺年度每月营业额走势图
    List<YearShopSaleCurveVO> getYearShopSaleCurve(LocalDate date, String shopId);

    // 店铺年度每月销售详情
    List<YearShopSaleDetailVO> getYearShopSaleDetail(LocalDate date, String shopId);

    // 年度员工业绩排行榜
    List<MonthStaffSaleTopVO> getYearStaffSaleTop(LocalDate date, String shopId);

    // 员工每周销售量TOP
    List<StaffHotGoodsTopNumVO> getStaffHotGoodsNumTop(LocalDate date, String staffId);

    // 员工每周销售额TOP
    List<StaffHotGoodsMoneyTopVO> getStaffHotGoodsMoneyTop(LocalDate date, String staffId);

    // 店铺每周单个店员销售统计（用于员工报表）
    StaffWeekSaleVO getStaffWeekSale(LocalDate date, String staffId);

    // 店铺每周热销商品榜(员工角度)
    List<DayHotGoodsTopVO> getStaffWeekHotGoodsTop(LocalDate date, String staffId);

    // 单一员工每日销售概览
    List<StaffDaySaleVO> getStaffDaySale(LocalDate date, String staffId);
}
