package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptLowStockWarning;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface RptLowStockWarningJpa extends JpaRepository<RptLowStockWarning, String> {
    List<RptLowStockWarning> findAllByDateAndShopId(LocalDate date,String shopId);
}
