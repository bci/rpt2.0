package com.bcb.rpt.server.service;

import com.bcb.rpt.server.dto.RptDragonTigerContentDTO;

public interface RptInsertService {


    String insertExitRecollection(RptDragonTigerContentDTO param);

    // 删除
    void delete(String id);
}
