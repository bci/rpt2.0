package com.bcb.rpt.server.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author Mr.Li
 * @description: 店铺每日各个商品销售量表 ( rpt_goods_sale_num )
 * @date 2020-07-07 15:59
 */

@Data
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
public class RptGoodsSaleNum {

    /**
     * id
     */
    @Id
    private String id;

    /**
     * 关联店铺表id
     */
    private String shopId;

    /**
     * 关联商户表id
     */
    private String merchantId;

    /**
     * 关联品类id
     */
    private String categoryId;

    /**
     * 关联商品id
     */
    private String goodsId;

    /**
     * 该店铺下某商品当天的销量
     */
    private Integer saleNum;

    /**
     * 该店铺下某商品的名称
     */
    private String name;

    /**
     * 该店铺下某商品的品类的名称
     */
    private String categoryName;

    /**
     * 该店铺下某商品当天的销售额
     */
    private Long saleMoney;

    /**
     * 数据统计日期(精确到天即可)
     */
    private LocalDateTime createTime;

    /**
     * 数据更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 版本控制锁（乐观锁）,默认值0
     */
    private Integer version;

    /**
     * 该条数据的归属时间
     */
    private LocalDate date;
}
