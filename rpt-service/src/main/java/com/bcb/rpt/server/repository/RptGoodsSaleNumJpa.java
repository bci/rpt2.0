package com.bcb.rpt.server.repository;

import com.bcb.rpt.server.entity.RptGoodsSaleNum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;


public interface RptGoodsSaleNumJpa extends JpaRepository<RptGoodsSaleNum, String> {
    List<RptGoodsSaleNum> findAllByDateAndShopId(LocalDate date, String shopId);

    // 每日热销商品榜
    @Query(nativeQuery = true, value = "SELECT " +
    "goods_id goods_id," +
    "sale_num sale_num," +
    "sale_money sale_money," +
    "name name " +
    "FROM rpt_goods_sale_num " +
    "where date = :date and shop_id = :shopId" +
    " order by sale_num desc ")
    List<Map<String, Object>> getDayHotGoodsTop(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 店铺每日产品销售额占比
    @Query(nativeQuery = true, value = "SELECT " +
            "category_name category_name," +
            "category_id category_id," +
            "sum(sale_money) sale_money," +
            "shop_id shop_id " +
            "FROM rpt_goods_sale_num " +
    "where date = :date and shop_id = :shopId " +
    " group by category_id, category_name, shop_id" +
    " order by sale_money desc")
    List<Map<String, Object>> getDayShopSalePro(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 店铺每日热销商品榜查询
//    @Query(nativeQuery = true, value = "SELECT " +
//            "goods_id goods_id," +
//            "sale_num sale_num," +
//            "sale_money sale_money," +
//            "name name " +
//            "FROM rpt_goods_sale_num " +
//            "where date = :date and shop_id = :shopId and name = ")
//    Map<String, Object> getFindGoodsSale(@Param("date")LocalDate date, @Param("shopId")String shopId);

    // 每周热销商品榜
    @Query(nativeQuery = true, value = "SELECT " +
            "goods_id goods_id," +
            "sum(sale_num) sale_num," +
            "sum(sale_money) sale_money," +
            "name name " +
            "FROM rpt_goods_sale_num " +
            "where date between :startDate and :endDate and shop_id = :shopId" +
            " group by goods_id, name" +
            " order by sale_num desc ")
    List<Map<String, Object>> getWeekHotGoodsTop(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                                 @Param("shopId")String shopId);

    // 店铺每周产品销售额占比
    @Query(nativeQuery = true, value = "SELECT " +
            "category_name category_name," +
            "category_id category_id," +
            "sum(sale_money) sale_money," +
            "shop_id shop_id " +
            "FROM rpt_goods_sale_num " +
            "where date between :startDate and :endDate and shop_id = :shopId " +
            " group by category_id, category_name, shop_id")
    List<Map<String, Object>> getWeekShopSalePro(@Param("startDate")LocalDate startDate, @Param("endDate")LocalDate endDate,
                                                 @Param("shopId")String shopId);

    // 月度每日热销商品
    @Query(nativeQuery = true, value = "select DATE_FORMAT(t3.date, '%m-%d') day, t3.name totalHotName " +
            "from (select t2.*,(select count(*) + 1 from rpt_goods_sale_num t1 where t1.date=t2.date and t1.shop_id=" +
            "t2.shop_id and t1.sale_num > t2.sale_num) rank_value from rpt_goods_sale_num t2 where t2.shop_id=:shopId " +
            "and date_format(t2.date, '%Y-%m')=date_format(:date, '%Y-%m')) t3 where t3.rank_value = 1 order by t3.date")
    List<Map<String, Object>> getMonthHotGoodsTop(@Param("date")LocalDate date, @Param("shopId")String shopId);


    // 年度每月热销商品
    @Query(nativeQuery = true, value = "SELECT mon,t3.NAME totalHotName " +
            "FROM (SELECT t2.*,(SELECT count( * ) + 1 FROM(SELECT t0.shop_id,date_format( t0.date, '%Y-%m' ) mon," +
            "t0.`name`,sum(sale_num )sale_num " +
            "FROM rpt_goods_sale_num t0 GROUP BY t0.shop_id,t0.`name`,mon ) t1 WHERE t1.mon = t2.mon AND t1.shop_id =" +
            " t2.shop_id AND t1.sale_num > t2.sale_num) rank_value FROM (SELECT t0.shop_id,date_format( t0.date,'%Y-%m')" +
            " mon,t0.`name`,sum(sale_num) sale_num FROM rpt_goods_sale_num t0 GROUP BY t0.shop_id,t0.`name`,mon ) t2 " +
            "WHERE t2.shop_id = :shopId AND SUBSTR(t2.mon FROM 1 FOR 4 ) = date_format(:date,'%Y')) t3 WHERE " +
            "t3.rank_value = 1 ORDER BY t3.mon")
    List<Map<String, Object>> getYearHotGoodsTop(@Param("date")LocalDate date, @Param("shopId")String shopId);
}
