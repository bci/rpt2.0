package com.bcb.user;

import com.bcb.commom.util.UuidUtil;
import com.bcb.rpt.server.util.RtpIdUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class UserApplicationTests {
    @Resource
    RtpIdUtil rtpIdUtil;

    @Test
    void contextLoads() {
    }

    @Test
    public void test(){
        System.out.println(UuidUtil.getUUid());
        System.out.println(rtpIdUtil.createId("test"));
    }
}